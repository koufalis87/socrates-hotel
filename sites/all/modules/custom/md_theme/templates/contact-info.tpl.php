<div class="widget widget_logo">
    <div class="widget-logo">
        <div class="img">
            <?php
				$default_value = theme_get_setting('ft_image_file_uploaded');
				$media_file = array('fid' => isset($default_value['fid']) ? intval($default_value['fid']) : 0);
				if ($media_file['fid'] && ($media_file = file_load($media_file['fid']))) {
				  $media_file = get_object_vars($media_file);
				}
			?>
            <a href="<?php print base_path(); ?>"><img src="<?php print file_create_url($media_file['uri']); ?>" alt="logo"></a>
        </div>
        <div class="text">
            <?php
            	$info = theme_get_setting('address_info');
			  	$info = explode('||', $info);
			  	array_pop($info);
			  	foreach ($info as $key => $value) {
					$info[$key] = $key != 0 ? substr($value, 1, -1) : substr($value, 0, -1);
			  	}
			  	$info = array_chunk($info, 4);
			?>
            <?php foreach ($info as $key => $value): ?>
                <p><i class="fa <?php print $value[0] ?>"></i>
                  <?php
                  if ($key == 1) print '<a href="tel:'.$value[3].'" target="_blank">' . $value[3] . '</a>';
                  elseif ($key == 2) print '<a href="mailto:'.$value[3].'" target="_blank">' . $value[3] . '</a>';
                  else print $value[3];
                  ?>
                </p>
            <?php endforeach; ?>
        </div>
    </div>
</div>
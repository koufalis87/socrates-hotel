<div class="social">
    <div class="social-content">
        <?php if (isset($social)): ?>
		  <?php foreach ($social as $key => $data): ?>
            <?php
              $icon = $data[1];
              $icon = explode('|', $icon);
            ?>
            <a href="<?php print $data[0]; ?>"><i class="fa <?php print $icon[1]; ?>"></i></a>
          <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
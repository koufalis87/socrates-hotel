<?php

/**
 * @file
 * Theme settings .
 */

function md_hillter_theme_settings_design(&$form, &$form_state) {
  global $base_url;
  $theme_default = variable_get('theme_default');

  $form['md_hillter_settings']['design'] = array(
    '#type' => 'fieldset',
    '#weight' => -4,
    '#prefix' => '<div id="md-design" class="md-tabcontent clearfix">',
    '#suffix' => '</div><!-- / #md-design -->',
  );

  $form['md_hillter_settings']['design']['design_htmllist'] = array(
    '#markup' => '<div id="md-content-sidebar" class="md-content-sidebar">
                        <ul class="clearfix">
                            <li><a href="#ds-header"><i class="fa-magic icon fa"></i>Header</a></li>
                            <li><a href="#ds-sidebar"><i class="fa-list-ul icon fa"></i>Sidebar</a></li>
                            <li><a href="#ds-contact-info"><i class="fa-envelope icon fa"></i>Contact</a></li>
                            <li><a href="#ds-contact"><i class="fa-info-circle icon fa"></i>Footer</a></li>
                            <li><a href="#ds-404"><i class="fa-chain-broken icon fa"></i>404</a></li>
                            <li><a href="#ds-comming"><i class="fa-clock-o icon fa"></i>Comming Soon</a></li>
                        </ul>
                    </div><!-- /.md-content-sidebar -->
                    <div class="md-content-main">',
    '#weight' => -15,
  );
  $form['md_hillter_settings']['design']['design_htmllistclose'] = array(
    '#markup' => '</div><!-- /.md-listleft -->',
    '#weight' => 15,
  );
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////// Color Skin //////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  $form['md_hillter_settings']['design']['ds_header'] = array(
    '#type' => 'fieldset',
    '#weight' => 1,
    '#prefix' => '<div id="ds-header"><div class="md-tabcontent-row">',
    '#suffix' => '    </div></div><!-- / #ds-general -->',
  );
  $form['md_hillter_settings']['design']['ds_header']['header_info'] = array(
    '#type' => 'textarea',
    '#format' => 'full_html',
    '#title' => t('Top header information'),
    '#default_value' => theme_get_setting('header_info'),
    '#attributes' => array(
      'class' => array('input-border big')
    ),
  );

  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////// Sidebar /////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  $form['md_hillter_settings']['design']['ds_sidebar'] = array(
    '#type' => 'fieldset',
    '#weight' => 1,
    '#prefix' => '<div id="ds-sidebar"><div class="md-tabcontent-row">',
    '#suffix' => '    </div></div><!-- / #ds-sidebar -->',
  );
  $form['md_hillter_settings']['design']['ds_sidebar']['sidebar_position'] = array(
    '#type' => 'select',
    '#title' => t('Choose Sidebar Position'),
    '#default_value' => theme_get_setting('sidebar_position'),
    '#options' => array(
      'right' => t('Right'),
      'left' => t('Left'),
      'no' => t('No Sidebar'),

    ),
    '#attributes' => array(
      'class' => array('select')
    ),
    '#prefix' => '<h3 class="md-tabcontent-title">Sidebar Settings</h3><div class="form-group" style="margin-bottom: 0">',
    '#suffix' => '</div>',
    '#field_prefix' => '<div class="md-selection medium">',
    '#field_suffix' => '</div>'
  );

  /////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////// Custom Address ////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  $form['md_hillter_settings']['design']['info_contact'] = array(
    '#type' => 'fieldset',
    '#weight' => 1,
    '#prefix' => '<div id="ds-contact-info"><div class="md-tabcontent-row">',
    '#suffix' => '    </div></div><!-- / #ds-general -->',
  );
  if (module_exists('icon')) {
    $icon_bundles = icon_bundles();
    $icons = array();
    foreach ($icon_bundles as $bundles) {
      $icons[] = md_icon_bundle_list($bundles);
    }
    $address_defaul = theme_get_setting('address_info')
      ? theme_get_setting('address_info')
      : 'icon fontello icon-phone,||,font_icon|icon-phone,||,Call Us,||,0307-567-890,||';

    $markup = '<ul id="sortable">
                    <li class="icon-sort draggable-item" id="li-0">
                      <div class="toolbar">
                          <a class="delete-icon" href="#">Delete</a>
                          <a class="clone-icon" href="#">Clone</a>
                      </div>
                      <div class="wrap-icon">
                      </div>
                    </li>
                  </ul>';
    $form_popup = '<div class="form-popup">
                    <ul >
                      <li><div class="choose-icon" data-value="font_icon|icon-glyph">Choose Icon<a href="#"><i class="icon fontello icon-glyph"></i></a></div></li>
                      <li><label>Enter Title</label><input class="form-text icon-title" type="text" name="title-icon" value="" /></li>
                      <li><label>Enter Text</label><input class="form-text icon-text" type="text" name="text-icon" value="" /></li>
                    </ul>
                  </div>';
    $form['md_hillter_settings']['design']['info_contact'] = array(
      '#type' => 'fieldset',
      '#weight' => 1,
      '#prefix' => '<div id="ds-contact-info"><div class="md-tabcontent-row">',
      '#suffix' => '    </div></div><!-- / #ds-general -->',
    );
    $form['md_hillter_settings']['design']['info_contact']['markup'] = array(
      '#type' => 'markup',
      '#markup' => '<div>Shit</div>',
    );
    $form['md_hillter_settings']['design']['info_contact']['address_info'] = array(
      '#type' => 'hidden',
      '#default_value' => $address_defaul,
      '#attributes' => array(
        'class' => array('icon-footer')
      )
    );
    $form['md_hillter_settings']['design']['info_contact']['popup_icon'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="icon-popup">' . drupal_render($icons) . '</div>',
      '#prefix' => '<div class="form-group" style="margin-top: 0">',
      '#suffix' => '</div>',
    );
    $form['md_hillter_settings']['design']['info_contact']['popup_form'] = array(
      '#type' => 'markup',
      '#prefix' => '<div class="form-group" style="margin-top: 0">',
      '#suffix' => '</div>',
      '#markup' => $form_popup
    );
    $form['md_hillter_settings']['design']['info_contact']['markup'] = array(
      '#type' => 'markup',
      '#markup' => $markup,
    );
  }
  else{
    $form['md_hillter_settings']['design']['info_contact']['markup'] = array(
      '#type' => 'markup',
      '#markup' => '<div>You need enable module Icon</div>',
    );
  }


  /////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////// Custom Address ////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  $form['md_hillter_settings']['design']['contact'] = array(
    '#type' => 'fieldset',
    '#weight' => 1,
    '#prefix' => '<div id="ds-contact"><div class="md-tabcontent-row">',
    '#suffix' => '    </div></div><!-- / #ds-general -->',
  );
  $form['md_hillter_settings']['design']['contact']['info_text'] = array(
    '#type' => 'textfield',
    '#format' => 'full_html',
    '#title' => t('Copyright'),
    '#default_value' => theme_get_setting('info_text'),
    '#attributes' => array(
      'class' => array('input-border big')
    ),
  );
  //FOOTER IMAGE SETTINGS
  $form['md_hillter_settings']['design']['contact']['footer_image'] = array(
    '#type' => 'container',
  );
  _build_form_upload_file($form['md_hillter_settings']['design']['contact']['footer_image'],'ft_image','Footer Image');


  /////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////// Page 404 //////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  $form['md_hillter_settings']['design']['404'] = array(
      '#type'                 => 'fieldset',
      '#weight'               => 1,
      '#prefix' => '<div id="ds-404"><div class="md-tabcontent-row">',
      '#suffix' => '    </div></div><!-- / #ds-general -->',
  );

  $form['md_hillter_settings']['design']['404']['404_title'] = array(
      '#type'                 => 'textfield',
      '#format'               => 'full_html',
      '#prefix'               => '<div class="md-tabcontent-header">
                                          <h3 class="md-tabcontent-title">404 Content</h3>
                                      </div><!-- /.md-row-description -->
                                      <div class="form-group">',
      '#suffix'               => '</div>',
      '#default_value'        =>  theme_get_setting('404_title') ? theme_get_setting('404_title') : t('404 Error!'),
      '#attributes'           => array(
          'class'             => array('input-border normal')
      )
  );
  $form['md_hillter_settings']['design']['404']['404_subtitle'] = array(
      '#type'                 => 'textarea',
      '#format'               => 'full_html',
      '#default_value'        =>  theme_get_setting('404_subtitle') ? theme_get_setting('404_subtitle') : t('The page you are looking for doesn\'t exist.'),
      '#attributes'           => array(
          'class'             => array('input-border big')
      )
  );
  $form['md_hillter_settings']['design']['404']['404_link'] = array(
      '#type'                 => 'textfield',
      '#format'               => 'full_html',
      '#default_value'        => theme_get_setting('404_link'),
      '#attributes'           => array(
          'class'             => array('input-border normal')
      )
  );
  //FOOTER IMAGE SETTINGS
  $form['md_hillter_settings']['design']['404']['404_image'] = array(
    '#type' => 'container',
  );
  _build_form_upload_file($form['md_hillter_settings']['design']['404']['404_image'],'404_image','404 Image');

  /////////////////////////////////////////////////////////////////////////////
  //////////////////////////// Page Comming Soon //////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  $form['md_hillter_settings']['design']['comming'] = array(
      '#type'                 => 'fieldset',
      '#weight'               => 1,
      '#prefix' => '<div id="ds-comming"><div class="md-tabcontent-row">',
      '#suffix' => '    </div></div><!-- / #ds-general -->',
  );

  $form['md_hillter_settings']['design']['comming']['comming_title'] = array(
      '#type'                 => 'textfield',
      '#format'               => 'full_html',
      '#prefix'               => '<div class="md-tabcontent-header">
                                          <h3 class="md-tabcontent-title">Comming Soon Content</h3>
                                      </div><!-- /.md-row-description -->
                                      <div class="form-group">',
      '#suffix'               => '</div>',
      '#default_value'        =>  theme_get_setting('comming_title') ? theme_get_setting('comming_title') : t('Comming Soon'),
      '#attributes'           => array(
          'class'             => array('input-border normal')
      )
  );
  $form['md_hillter_settings']['design']['comming']['comming_subtitle'] = array(
      '#type'                 => 'textarea',
      '#format'               => 'full_html',
      '#default_value'        =>  theme_get_setting('comming_subtitle') ? theme_get_setting('comming_subtitle') : t('We are working harder'),
      '#attributes'           => array(
          'class'             => array('input-border big')
      )
  );
  $form['md_hillter_settings']['design']['comming']['comming_follow'] = array(
      '#type'                 => 'textfield',
      '#format'               => 'full_html',
      '#default_value'        => theme_get_setting('comming_follow'),
      '#attributes'           => array(
          'class'             => array('input-border normal')
      )
  );
  $array = array();
  for ($i = 2015; $i < 2101; $i++) {
      $array[$i] = $i;
  }
  $form['md_hillter_settings']['design']['comming']['comming_year'] = array(
      '#type'                 => 'select',
      '#title'                => 'Select End Year',
      '#description'          => 'Select the year when your website is launched.',
      '#options'              => $array,
      '#default_value'        => theme_get_setting('comming_year'),
      '#attributes'           => array(
          'class'             => array('select')
      ),
      '#prefix'               => '<div class="form-group">',
      '#suffix'               => '</div>',
      '#field_prefix'         => '<div class="md-selection small" style="margin: 10px 0 5px;">',
      '#field_suffix'         => '</div>',
  );
  $form['md_hillter_settings']['design']['comming']['comming_month'] = array(
      '#type'                 => 'select',
      '#title'                => 'Select End Month',
      '#description'          => 'Select the month when your website is launched.',
      '#options'              => range(1, 12),
      '#default_value'        => theme_get_setting('comming_month'),
      '#attributes'           => array(
          'class'             => array('select')
      ),
      '#prefix'               => '<div class="form-group">',
      '#suffix'               => '</div>',
      '#field_prefix'         => '<div class="md-selection small" style="margin: 10px 0 5px;">',
      '#field_suffix'         => '</div>',
  );
  $form['md_hillter_settings']['design']['comming']['comming_day'] = array(
      '#type'                 => 'select',
      '#title'                => 'Select End Day',
      '#description'          => 'Select the day when your website is launched.',
      '#options'              => range(1, 31),
      '#default_value'        => theme_get_setting('comming_day'),
      '#attributes'           => array(
          'class'             => array('select')
      ),
      '#prefix'               => '<div class="form-group">',
      '#suffix'               => '</div>',
      '#field_prefix'         => '<div class="md-selection small" style="margin: 10px 0 5px;">',
      '#field_suffix'         => '</div>',
  );
  //FOOTER IMAGE SETTINGS
  $form['md_hillter_settings']['design']['comming']['comming_image'] = array(
    '#type' => 'container',
  );
  _build_form_upload_file($form['md_hillter_settings']['design']['comming']['comming_image'],'comming_image','Comming Soon Image');
}

/**
 * Handler room filters
 *  
 * @param {type} $
 * @returns {undefined}
 */
(function ($) {


    Drupal.behaviors.mdBookingFilter = {
        attach: function (context, settings) {
            var defaultRooms = Drupal.settings.md_hillter.rooms;
            var formFilter = $("#views-exposed-form-rooms-room-v1");
            
            formFilter.find('.btn-filter').once('filter', function() {
                formFilter.find('.btn-filter').click(function (event) {
                    var children = formFilter.find('input[name="field_room_children_value"]');
                    var adult = formFilter.find('input[name="field_room_max_person_value"]');

                    var minChildren = findMinChildren();
                    var minAdult = findMinAdult();

                    children.val(minChildren);
                    adult.val(minAdult);


                    event.preventDefault();
                    formFilter.find('input[type="submit"]').trigger('click');
                });
            })
            

            function findMinChildren() {
                // get started at the first.
                var $return = parseInt($(".rooms-children-" + 1).val());

                for (var i = 1; i <= defaultRooms; ++i) {
                    var element = $(".rooms-children-" + i);

                    if (element.is(":visible")) {
                        var value = parseInt(element.val());
                        if (value < $return) {
                            $return = value;
                        }
                    }
                }
                return $return;
            }

            function findMinAdult() {
                // get started at the first.
                var $return = parseInt($(".rooms-adult-" + 1).val());

                for (var i = 1; i <= defaultRooms; ++i) {
                    var element = $(".rooms-adult-" + i);

                    if (element.is(":visible")) {
                        var value = parseInt(element.val());
                        if (value < $return) {
                            $return = value;
                        }
                    }
                }
                return $return;
            }

            
             // Add the behavior to each region select list.
            formFilter.find('select.rooms-choose').once('room-filter', function () {
                formFilter.find('.rooms-choose').change(function () {
                    var rooms = $(this).val();
                    for (var i = 1; i <= defaultRooms; ++i) {
                        if (i <= rooms) {
                            formFilter.find('.rooms-group-' + i).show();
                        } else {
                            formFilter.find('.rooms-group-' + i).hide();
                        }
                    
                    }
                });
                formFilter.find('.rooms-choose').trigger('change');
            });
            
            // End attach
        }
    }


})(jQuery);

/**
 * The file handle booking.
 * 
 *  - Limit arrive & depature date by availability calendars
 *  - Commerce quatity = number of room * (depature - arrive);
 *  
 * @param {type} $
 * @returns {undefined}
 */
(function ($) {
  Drupal.behaviors.mdBooking = {
    attach: function (context, settings) {
      var form_commerce = $('.commerce-add-to-cart');
      
      // Use datapicker with availability calendars.
      $( ".awe-calendar.has-filter" ).datepicker({
				prevText: '<i class="hillter-icon-left-arrow"></i>',
				nextText: '<i class="hillter-icon-right-arrow"></i>',
				buttonImageOnly: false,
				minDate: 0,
        beforeShowDay: function(date){
            var string = jQuery.datepicker.formatDate('mm/d/yy', date);
            return [ Drupal.settings.md_hillter.unavailable.indexOf(string) == -1 ];
        }
			});
      
      // Extra UI for datapicker.
      $('.awe-calendar.has-filter').each(function() {
				var aweselect = $(this);
				aweselect.wrap('<div class="awe-calendar-wrapper"></div>');
				aweselect.after('<i class="hillter-icon-calendar"></i>');
			});
      
      form_commerce.find('.awe-calendar.arrive').once('processed-add-to-cart', function() {
        form_commerce.find('.awe-calendar.arrive').change(function(event) {
          form_commerce.find('input[name="line_item_fields\[field_line_arrive\]\[und\]\[0\]\[value\]\[date\]"]').val($(this).val());
        });
      });
      
      form_commerce.find('.awe-calendar.depature').once('processed-add-to-cart', function() {
        form_commerce.find('.awe-calendar.depature').change(function(event) {
          form_commerce.find('input[name="line_item_fields\[field_line_departure\]\[und\]\[0\]\[value\]\[date\]"]').val($(this).val());
        });
      });
      
      // On book now submit event
      form_commerce.find('.awe-btn').once('processed-add-to-cart', function() {
        form_commerce.find('.awe-btn').click(function (event) {
          var numberOfQuantity = calculateQuantity();
          if (numberOfQuantity > 0) {
            var quantity = form_commerce.find('input[name="quantity"]');
//            quantity.val(numberOfQuantity);
          }
          event.preventDefault();
          form_commerce.find('input[type="submit"]').trigger("click");
        });
      });
    
      // Calculate base on date of books and number of room
      function calculateQuantity() {
        var quantity = form_commerce.find('input[name="quantity"]');
        var arrive = form_commerce.find('.arrive');
        var depature = form_commerce.find('.depature');
        
        var arriveDate = arrive.datepicker("getDate");
        var depatureDate = depature.datepicker("getDate");

        var days = daysBetween(arriveDate, depatureDate);
        return parseInt(quantity.val()) * parseInt(days);
      }
      
      // Calculate number of date bettwen ARRIVE & DEPATURE date.
      daysBetween = function (date1, date2) {
        //Get 1 day in milliseconds
        var one_day = 1000 * 60 * 60 * 24;

        // Convert both dates to milliseconds
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();

        // Calculate the difference in milliseconds
        var difference_ms = date2_ms - date1_ms;

        // Convert back to days and return
        return Math.round(difference_ms / one_day);
      }
      
      // End attach
    }
  }


})(jQuery);

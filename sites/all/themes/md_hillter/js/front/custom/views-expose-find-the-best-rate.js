(function ($) {
  Drupal.behaviors.findTheBestRate = {
    attach: function (context, settings) {
        var formFilter = $('#views-exposed-form-rooms-room-v1');
        
        formFilter.find('.btn-filter').once('filter', function() {
            formFilter.find('.btn-filter').click(function (event) {
                var children = formFilter.find('input[name="field_room_children_value"]');
                var adult    = formFilter.find('input[name="field_room_max_person_value"]');

                var minChildren = parseInt(formFilter.find('.rooms-children').val());
                var minAdult    = parseInt(formFilter.find('.rooms-adults').val());

                children.val(minChildren);
                adult.val(minAdult);

                event.preventDefault();
                formFilter.find('input[type="submit"]').trigger('click');
            });
        });
      
      // End attach
    }
  }


})(jQuery);

<?php
include_once './' . drupal_get_path('theme', 'md_hillter') . '/inc/front/html.preprocess.inc';
include_once './' . drupal_get_path('theme', 'md_hillter') . '/inc/front/page.preprocess.inc';
include_once './' . drupal_get_path('theme', 'md_hillter') . '/inc/front/function.theme.inc';

/**
 * Implements theme_menu_tree().
 */
function md_hillter_menu_tree__main_menu($variables) {
	return '<ul class="menu">' . $variables['tree'] . '</ul>';
}

function md_hillter_menu_tree__menu_top_menu($variables) {
	return '<ul class="menu">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_menu_link__[MENU NAME].
 */
function md_hillter_menu_link__main_menu($variables) {

  $element = $variables['element'];
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  $sub_menu = drupal_render($element['#below']);
  $sub_menu = str_replace('class="menu"','class="sub-menu"',$sub_menu);
  
  if($element['#below']) {
	$title = $element['#title'] . ' <span class="fa fa-caret-down"></span>';
	$output = l($title, $element['#href'], array('html' => TRUE));
	return '<li ' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
  }
  else {
	return '<li>' . $output . "</li>\n";
  }
}

function md_hillter_menu_link__menu_top_menu($variables) {

  $element = $variables['element'];
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  $sub_menu = drupal_render($element['#below']);
  $sub_menu = str_replace('class="menu"','class="sub-menu"',$sub_menu);
  
  if($element['#below']) {
	$title = $element['#title'] . ' <span class="fa fa-caret-down"></span>';
	$output = l($title, $element['#href'], array('html' => TRUE));
	return '<li ' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
  }
  else {
	return '<li>' . $output . "</li>\n";
  }
}

function md_hillter_css_alter(&$css) {
  unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
  unset($css[drupal_get_path('module', 'rooms') . '/css/rooms_date_range_fields.css']);
  unset($css[drupal_get_path('module', 'rooms') . '/css/rooms_modal.css']);
  unset($css[drupal_get_path('module', 'rooms') . '/css/rooms_options_widget.css']);
  unset($css[drupal_get_path('module', 'rooms') . '/css/rooms_ui.css']);
  unset($css[drupal_get_path('module', 'rooms') . '/modules/rooms_booking_manager/css/rooms_booking_search.css']);
  unset($css[drupal_get_path('module', 'rooms') . '/modules/rooms_booking_manager/css/rooms_sticky_booking_legend.css']);
  unset($css[drupal_get_path('module', 'date') . '/date_api/date.css']);
}

/**
 * Implement hook_theme().
 */
function md_hillter_theme($existing, $type, $theme, $path) {
  return array(
    // Override commerce add to cart form template
    'commerce_cart_add_to_cart_form' => array(
      'render element' => 'form',
      'template' => 'commerce-cart-add-to-cart-form',
      'path' => drupal_get_path('theme', 'md_hillter') . '/templates/forms', 
    ),
    // Override commerce add to cart form template
    'views_expose_find_the_best_rate' => array(
      'render element' => 'form',
      'template' => 'views-expose-find-the-best-rate',
      'path' => drupal_get_path('theme', 'md_hillter') . '/templates/forms', 
    ),
  );
}

/**
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function md_hillter_form_alter(&$form, &$form_state, $form_id) {
    switch ($form_id) {
	  case 'search_block_form':
		  $form['search_block_form']['#attributes']['class'][] = 'filed-text';
		  $form['search_block_form']['#attributes']['placeholder'] = t('Search page');
		  $form['actions']['submit']['#attributes'] = array(
			  'class' => array('awe-btn awe-btn-12'),
		  );
		  break;
	  case 'simplenews_block_form_31':
		  $form['mail']['#title_display'] = 'invisible';
		  $form['mail']['#attributes']['placeholder'] = t('Your email address');
		  $form['mail']['#attributes']['class'][] = t('input-text');
		  
		  $form['submit']['#attributes']['class'][] = 'awe-btn';
		  $form['submit']['#value'] = t('SIGN UP');
		  break;
	}
}

/**
 * @see template_preprocess_commerce_cart_add_to_cart_form().
 */
function md_hillter_preprocess_commerce_cart_add_to_cart_form(&$vars) {
  // Add html class & update text
  $vars['form']['attributes']['field_room_package']['#attributes']['class'][] = 'awe-select';
  $vars['form']['attributes']['field_room_package']['#title'] = t('Type');
  
  $vars['form']['quantity']['#title'] = t('Room');
  $vars['form']['quantity']['#attributes']['class'][] = 'field-text';
  
  // Get
  $node = menu_get_object();
  $date_formates = array();
  
  if(isset($node->field_room_check_available['und'])) {
	  if ($node && $node->field_room_check_available['und'][0]['enabled']) {
		$cid = $node->field_room_check_available['und'][0]['cid'];
		$from = new DateTime;
		$to = clone $from;        
		$to->modify('+100 day');
		$format_date = 'm/d/Y';
		
		module_load_include('inc', 'availability_calendar', 'availability_calendar');
		$unavailable = availability_calendar_get_availability($cid, $from, $to);
	
		foreach ($unavailable as $dateString => $enable) {
		  if ($enable) {
			$myDateTime = DateTime::createFromFormat(AC_ISODATE, $dateString);
			$date_formates[] = $myDateTime->format($format_date);
		  }
		}
	  }
  }

  drupal_add_js(array('md_hillter' => array('unavailable' => $date_formates)), 'setting');
  drupal_add_js(drupal_get_path('theme', 'md_hillter') . '/js/front/custom/md_booking.js');
}

function md_hillter_form_commerce_cart_add_to_cart_form_alter(&$form, &$form_state, $form_id) {
  $form['#validate'][] = 'md_hillter_validate_booking';
  $form['line_item_fields']['field_line_arrive']['und'][0]['#default_value'] = array();
  $form['line_item_fields']['field_line_departure']['und'][0]['#default_value'] = array();
  $form['form']['quantity']['#attributes']['class'][] = 'field-text';
}

function md_hillter_validate_booking(&$form, &$form_state) {
  $has_date = TRUE;
  if (empty($form_state['values']['line_item_fields']['field_line_arrive']['und'][0]['value'])) {
    $has_date = FALSE;
  }
  else {
    $arrive = $form_state['values']['line_item_fields']['field_line_arrive']['und'][0]['value'];
  }
  if (empty($form_state['values']['line_item_fields']['field_line_departure']['und'][0]['value'])) {
    $has_date = FALSE;
  }
  else {
    $departure = $form_state['values']['line_item_fields']['field_line_departure']['und'][0]['value'];
  }
  
  if (!$has_date) {
    form_set_error('line_item_fields', t('Please choose arrive & departure date'));
  }
  else {
    if (strtotime($departure) < strtotime($arrive)) {
      form_set_error('line_item_fields', t('Departure date must larger arrive date'));
    }
  }  
}

function md_hillter_form_comment_form_alter(&$form, &$form_state) {
	unset($form['actions']['preview']);
	
	$form['author']['name']['#attributes'] = array('placeholder' => t('Name'));
	$form['author']['_author']['#title_display'] = 'invisible';
	
	if(user_is_logged_in()) {
		$form['author']['_author']['#prefix'] = '<div class="col-sm-6">';
		$form['author']['_author']['#suffix'] = '</div>';
		$form['field_blog_comment_email']['und'][0]['email']['#prefix'] = '<div class="col-sm-12">';
		$form['field_activity_comment_email']['und'][0]['email']['#prefix'] = '<div class="col-sm-12">';
		$form['field_event_comment_email']['und'][0]['email']['#prefix'] = '<div class="col-sm-12">';
	} else {
		$form['author']['name']['#prefix'] = '<div class="col-sm-6">';
		$form['author']['name']['#suffix'] = '</div>';
		$form['field_blog_comment_email']['und'][0]['email']['#prefix'] = '<div class="col-sm-6">';
		$form['field_activity_comment_email']['und'][0]['email']['#prefix'] = '<div class="col-sm-6">';
		$form['field_event_comment_email']['und'][0]['email']['#prefix'] = '<div class="col-sm-6">';
	}
	$form['author']['name']['#title_display'] = 'invisible';
	$form['author']['name']['#attributes']['class'][] = 'field-text';
	
	$form['field_blog_comment_email']['und'][0]['email']['#attributes'] = array('placeholder' => t('Email'));
	$form['field_blog_comment_email']['und'][0]['email']['#title_display'] = 'invisible';
	$form['field_blog_comment_email']['und'][0]['email']['#attributes']['class'][] = 'field-text';
	$form['field_blog_comment_email']['und'][0]['email']['#suffix'] = '</div>';
	
	$form['field_activity_comment_email']['und'][0]['email']['#attributes'] = array('placeholder' => t('Email'));
	$form['field_activity_comment_email']['und'][0]['email']['#title_display'] = 'invisible';
	$form['field_activity_comment_email']['und'][0]['email']['#attributes']['class'][] = 'field-text';
	$form['field_activity_comment_email']['und'][0]['email']['#suffix'] = '</div>';
	
	$form['field_event_comment_email']['und'][0]['email']['#attributes'] = array('placeholder' => t('Email'));
	$form['field_event_comment_email']['und'][0]['email']['#title_display'] = 'invisible';
	$form['field_event_comment_email']['und'][0]['email']['#attributes']['class'][] = 'field-text';
	$form['field_event_comment_email']['und'][0]['email']['#suffix'] = '</div>';
	
	$form['subject']['#attributes'] = array('placeholder' => t('Subject'));
	$form['subject']['#title_display'] = 'invisible';
	$form['subject']['#attributes']['class'][] = 'field-text';
	$form['subject']['#prefix'] = '<div class="col-sm-12">';
	$form['subject']['#suffix'] = '</div>';
	
	$form['comment_body']['und'][0]['value']['#attributes'] = array('placeholder' => t('Your comment'));
	$form['comment_body']['und'][0]['value']['#title_display'] = 'invisible';
	$form['comment_body']['und'][0]['value']['#attributes']['class'][] = 'field-textarea';
	$form['comment_body']['und'][0]['value']['#prefix'] = '<div class="col-sm-12">';
	$form['comment_body']['und'][0]['value']['#suffix'] = '</div>';
	
	$form['actions']['submit']['#value'] = 'SUBMIT COMMENT';
	$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-14';
	$form['actions']['submit']['#prefix'] = '<div class="col-sm-12">';
	$form['actions']['submit']['#suffix'] = '</div>';
}

/**
 * Override contact form template
 */
function md_hillter_form_webform_client_form_80_alter(&$form, &$form_state) {
  	$form['submitted']['arrival_date']['#title_display'] = 'invisible';
	$form['submitted']['arrival_date']['#attributes'] = array('placeholder' => t('Arrival Date'));
	$form['submitted']['arrival_date']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['arrival_date']['#attributes']['class'][] = 'awe-calendar';
	$form['submitted']['arrival_date']['#prefix'] = '<div class="col-md-3 col-sm-6">';
	$form['submitted']['arrival_date']['#suffix'] = '</div>';
  
	$form['submitted']['departure_date']['#title_display'] = 'invisible';
	$form['submitted']['departure_date']['#attributes'] = array('placeholder' => t('Departure Date'));
	$form['submitted']['departure_date']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['departure_date']['#attributes']['class'][] = 'awe-calendar';
	$form['submitted']['departure_date']['#prefix'] = '<div class="col-md-3 col-sm-6">';
	$form['submitted']['departure_date']['#suffix'] = '</div>';
	
	$form['submitted']['start_time']['#title_display'] = 'invisible';
	$form['submitted']['start_time']['#attributes'] = array('placeholder' => t('Start time'));
	$form['submitted']['start_time']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['start_time']['#attributes']['class'][] = 'awe-input';
	$form['submitted']['start_time']['#prefix'] = '<div class="col-md-3 col-sm-6">';
	$form['submitted']['start_time']['#suffix'] = '</div>';
  
	$form['submitted']['adults']['#title_display'] = 'invisible';
	$form['submitted']['adults']['#attributes']['class'][] = 'awe-select';
	$form['submitted']['adults']['#prefix'] = '<div class="col-md-3 col-sm-6">';
	$form['submitted']['adults']['#suffix'] = '</div>';
	
	$form['submitted']['message']['#title_display'] = 'invisible';
	$form['submitted']['message']['#default_value'] = 'Request when reservation';
	$form['submitted']['message']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['message']['#attributes']['class'][] = 'awe-teaxtarea';
	$form['submitted']['message']['#prefix'] = '<div class="col-md-12 col-sm-12">';
	$form['submitted']['message']['#suffix'] = '</div>';
  
	$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
	$form['actions']['submit']['#prefix'] = '<div class="col-md-12 col-sm-12 text-center">';
	$form['actions']['submit']['#suffix'] = '</div>';
}

function md_hillter_form_webform_client_form_96_alter(&$form, &$form_state) {
  	$form['submitted']['name']['#title_display'] = 'invisible';
	$form['submitted']['name']['#attributes'] = array('placeholder' => t('Name'));
	$form['submitted']['name']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['name']['#attributes']['class'][] = 'field-text';
	$form['submitted']['name']['#prefix'] = '<div class="col-sm-6">';
	$form['submitted']['name']['#suffix'] = '</div>';
  
	$form['submitted']['email']['#title_display'] = 'invisible';
	$form['submitted']['email']['#attributes'] = array('placeholder' => t('Email'));
	$form['submitted']['email']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['email']['#attributes']['class'][] = 'field-text';
	$form['submitted']['email']['#prefix'] = '<div class="col-sm-6">';
	$form['submitted']['email']['#suffix'] = '</div>';
	
	$form['submitted']['subject']['#title_display'] = 'invisible';
	$form['submitted']['subject']['#attributes'] = array('placeholder' => t('Subject'));
	$form['submitted']['subject']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['subject']['#attributes']['class'][] = 'field-text';
	$form['submitted']['subject']['#prefix'] = '<div class="col-sm-12">';
	$form['submitted']['subject']['#suffix'] = '</div>';
	
	$form['submitted']['message']['#title_display'] = 'invisible';
	$form['submitted']['message']['#default_value'] = 'Write what do you want';
	$form['submitted']['message']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['message']['#attributes']['class'][] = 'field-textarea';
	$form['submitted']['message']['#prefix'] = '<div class="col-sm-12">';
	$form['submitted']['message']['#suffix'] = '</div>';
  
	$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
	$form['actions']['submit']['#prefix'] = '<div class="col-sm-6">';
	$form['actions']['submit']['#suffix'] = '</div>';
}

function md_hillter_form_webform_client_form_97_alter(&$form, &$form_state) {
  	$form['submitted']['name']['#title_display'] = 'invisible';
	$form['submitted']['name']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['name']['#attributes']['class'][] = 'field-text';
	$form['submitted']['name']['#prefix'] = '<div class="form-field">';
	$form['submitted']['name']['#suffix'] = '</div>';
  
	$form['submitted']['email']['#title_display'] = 'invisible';
	$form['submitted']['email']['#attributes'] = array('placeholder' => t('Email Address'));
	$form['submitted']['email']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['email']['#attributes']['class'][] = 'field-text';
	$form['submitted']['email']['#prefix'] = '<div class="form-field">';
	$form['submitted']['email']['#suffix'] = '</div>';
	
	$form['submitted']['location']['#title_display'] = 'invisible';
	$form['submitted']['location']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['location']['#attributes']['class'][] = 'field-text';
	$form['submitted']['location']['#prefix'] = '<div class="form-field">';
	$form['submitted']['location']['#suffix'] = '</div>';
	
	$form['submitted']['title']['#title_display'] = 'invisible';
	$form['submitted']['title']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['title']['#attributes']['class'][] = 'field-text';
	$form['submitted']['title']['#prefix'] = '<div class="form-field">';
	$form['submitted']['title']['#suffix'] = '</div>';
	
	$form['submitted']['message']['#title_display'] = 'invisible';
	$form['submitted']['message']['#default_value'] = 'Your Testimonial';
	$form['submitted']['message']['#attributes']['contenteditable'][] = 'true';
	$form['submitted']['message']['#attributes']['class'][] = 'field-textarea';
	$form['submitted']['message']['#prefix'] = '<div class="form-field">';
	$form['submitted']['message']['#suffix'] = '</div>';
  
	$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
	$form['actions']['submit']['#prefix'] = '<div class="form-field">';
	$form['actions']['submit']['#suffix'] = '</div>';
}

function md_hillter_panels_default_style_render_region($vars) {
  	$output = '';
  	$output .= implode('', $vars['panes']);
  	return $output;
}

function md_hillter_form_user_login_alter(&$form, &$form_state) {
	$form['#attributes']['class'][] = 'form';
	$form['name']['#attributes']['class'][] = 'field-text';	
	$form['pass']['#attributes']['class'][] = 'field-text';
	$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
}

function md_hillter_form_user_register_form_alter(&$form, &$form_state) {
	$form['#attributes']['class'][] = 'form';
	$form['account']['name']['#attributes']['class'][] = 'field-text';
	$form['account']['mail']['#attributes']['class'][] = 'field-text';
	$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
}

function md_hillter_form_user_pass_alter(&$form, &$form_state) {
	$form['#attributes']['class'][] = 'form';
	$form['name']['#attributes']['class'][] = 'field-text';
	$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
}

function md_hillter_form_views_form_commerce_cart_form_default_alter(&$form, &$form_state) {
	$form['edit_delete'][0]['#attributes']['class'][] = 'awe-btn awe-btn-default';
	
	$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
	$form['actions']['submit']['#value'] = 'Update Cart';
	
	$form['actions']['checkout']['#attributes']['class'][] = 'awe-btn awe-btn-13';
	$form['actions']['checkout']['#value'] = 'Process to Checkout';
}

function md_hillter_form_commerce_checkout_form_checkout_alter(&$form, &$form_state) {
	$form['cart_contents']['#attributes']['class'][] = 'hidden';
	
	$form['#attributes']['class'][] = 'reservation-billing-detail';
	$form['customer_profile_billing']['#prefix'] = '<h4>' . $form['customer_profile_billing']['#title'] . '</h4>';
	
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['country']['#attributes']['class'][] = 'awe-select';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['locality_block']['administrative_area']['#attributes']['class'][] = 'awe-select';
	
	$form['buttons']['continue']['#attributes']['class'][] = 'awe-btn awe-btn-13';
	
	unset($form['cart_contents']['cart_contents_view']);
}

function md_hillter_form_commerce_checkout_form_review_alter(&$form, &$form_state) {
	$form['commerce_payment']['payment_details']['credit_card']['number']['#attributes']['class'][] = 'field-text';
	
	$form['commerce_payment']['payment_details']['credit_card']['exp_month']['#attributes']['class'][] = 'awe-select';
	
	$form['commerce_payment']['payment_details']['credit_card']['exp_year']['#attributes']['class'][] = 'awe-select';
	
	$form['buttons']['continue']['#attributes']['class'][] = 'awe-btn awe-btn-13';
	$form['buttons']['back']['#prefix'] = '&nbsp;';
	$form['buttons']['back']['#attributes']['class'][] = 'awe-btn awe-btn-default';
}

function md_hillter_form_views_form_booking_cart_form_default_alter(&$form, &$form_state) {
	$total = count($form['edit_delete'])-1;
	for($i=0; $i < $total; $i++) {
		$form['edit_delete'][$i]['#attributes']['class'][] = 'awe-btn awe-btn-default';	
	}
	
	$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
	$form['actions']['submit']['#value'] = 'Update Cart';
	
	$form['actions']['checkout']['#attributes']['class'][] = 'awe-btn awe-btn-13';
	$form['actions']['checkout']['#value'] = 'Process to Checkout';
}

function md_hillter_addressfield_container($variables) {
  $element = $variables['element'];
  
  $element['#children'] = trim($element['#children']);
  if (strlen($element['#children']) > 0) {
    $output = '<' . $element['#tag'] . drupal_attributes($element['#attributes']) . '>';
    
    // add title here
    if (isset($element['#title'] )) {
      $output .= '<label>' . $element['#title'] . '</label>' . $element['#children'];
    }
    else {
      $output .= $element['#children'];
    }
    $output .= '</' . $element['#tag'] . ">";
    return $output;
  }
  else {
    return '';
  }
}

function md_hillter_preprocess_maintenance_page(&$vars) {
	$path_theme = drupal_get_path('theme', 'md_hillter');

  	drupal_add_js("{$path_theme}/js/front/lib/jquery-ui.min.js");
	drupal_add_js("{$path_theme}/js/front/lib/bootstrap.min.js");
	drupal_add_js("{$path_theme}/js/front/lib/bootstrap-select.js");
	drupal_add_js("https://maps.googleapis.com/maps/api/js?v=3.exp&amp;signed_in=true");
	drupal_add_js("{$path_theme}/js/front/lib/isotope.pkgd.min.js");
	drupal_add_js("{$path_theme}/js/front/lib/jquery.themepunch.revolution.min.js");
	drupal_add_js("{$path_theme}/js/front/lib/jquery.themepunch.tools.min.js");
	drupal_add_js("{$path_theme}/js/front/lib/owl.carousel.js");
	drupal_add_js("{$path_theme}/js/front/lib/jquery.appear.min.js");
	drupal_add_js("{$path_theme}/js/front/lib/jquery.countTo.js");
	drupal_add_js("{$path_theme}/js/front/lib/jquery.countdown.min.js");
	drupal_add_js("{$path_theme}/js/front/lib/jquery.parallax-1.1.3.js");
	drupal_add_js("{$path_theme}/js/front/lib/jquery.magnific-popup.min.js");
	drupal_add_js("{$path_theme}/js/front/lib/SmoothScroll.js");
	drupal_add_js("{$path_theme}/js/front/scripts.js");
  
	//  drupal_add_css();
	drupal_add_css("{$path_theme}/css/front/lib/font-awesome.min.css");
	drupal_add_css("{$path_theme}/css/front/lib/font-hilltericon.css");
	drupal_add_css("{$path_theme}/css/front/lib/bootstrap.min.css");
	drupal_add_css("{$path_theme}/css/front/lib/owl.carousel.css");
	drupal_add_css("{$path_theme}/css/front/lib/jquery-ui.min.css");
	drupal_add_css("{$path_theme}/css/front/lib/magnific-popup.css");
	drupal_add_css("{$path_theme}/css/front/lib/settings.css");
	drupal_add_css("{$path_theme}/css/front/lib/bootstrap-select.min.css");
	drupal_add_css("{$path_theme}/css/front/style.css");
	
	md_icon_load();
}

/**
 * @see template_preprocess_panels_pane().
 */
function md_hillter_preprocess_panels_pane(&$vars) {
  $pane = $vars['pane'];
  $display = $vars['display'];
  $vars['subtitle'] = variable_get("subtitle__{$display->uuid}_{$pane->subtype}", '');
  $vars['bg_effect'] = variable_get("bg_effect__{$display->uuid}_{$pane->subtype}", '');
  $bg_image = variable_get("bg_image__{$display->uuid}_{$pane->subtype}", '');
  if($bg_image) {
  	if(isset($bg_image['bg_image_path']))
		$vars['bg_image'] = md_theme_setting_check_path($bg_image['bg_image_path']);
  } else {
  	$vars['bg_image'] = "";
  }
}

function md_hillter_form_rooms_booking_availability_search_form_block_alter(&$form, &$form_state) {
	$form['rooms_date_range']['rooms_start_date']['#attributes']['class'][] = 'awe-calendar arrival-calendar';
	$form['rooms_date_range']['rooms_end_date']['#attributes']['class'][] = 'awe-calendar departure-calendar';
	
	$form['rooms_fieldset']['group_size_adults:1']['#attributes']['class'][] = 'awe-select';
	$form['rooms_fieldset']['group_size_children:1']['#attributes']['class'][] = 'awe-select';
	
	if(drupal_is_front_page()) {
		$form['#prefix'] = '<section class="section-check-availability"><div class="container"><div class="check-availability"><div class="row"><div class="col-lg-3"><h2>CHECK AVAILABILITY</h2></div><div class="col-lg-9"><div class="availability-form">';
		$form['#suffix'] = '</div></div></div></div></div></section>';
		
		$form['actions']['#prefix'] = '<div class="vailability-submit">';
		$form['actions']['#suffix'] = '</div>';
		$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
		$form['actions']['submit']['#value'] = t('FIND THE BEST RATE');
		unset($form['rooms_fieldset']['table_header']);
		unset($form['rooms_fieldset']['table_body']);
		unset($form['rooms_fieldset']['rooms_label:1']);
		unset($form['rooms_fieldset']['group_size_adults:1']['#prefix']);
		unset($form['rooms_fieldset']['group_size_adults:1']['#suffix']);
		unset($form['rooms_fieldset']['group_size_children:1']['#prefix']);
		unset($form['rooms_fieldset']['group_size_children:1']['#suffix']);
		unset($form['rooms_fieldset']['table_row:1']);
		unset($form['rooms_fieldset']['table_end']);
		unset($form['rooms_date_range']['rooms_start_date']['#title']);
		unset($form['rooms_date_range']['rooms_end_date']['#title']);
		
		$form['rooms_date_range']['rooms_start_date']['#suffix'] = '<div class="arrival element-invisible">' . t('Arrival Date') . '</div></div>';
		$form['rooms_date_range']['rooms_end_date']['#suffix'] = '<div class="departure element-invisible">' . t('Departure Date') . '</div></div></div>';
	} else {
		$node = menu_get_object();
		if (isset($node)) {
			if($node->type == "unit_description") {
				$rooms = _md_rooms_get_rooms_detail($node->nid);
				
				$form['rooms_fieldset']['group_size_adults:1']['#title'] = t('Adults');
				$form['rooms_fieldset']['group_size_children:1']['#title'] = t('Children');
				$form['unit_type']['#default_value'] = $rooms['type'];
				$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
				$form['actions']['submit']['#value'] = t('CHECK NOW');
				
				unset($form['rooms_fieldset']['table_header']);
				unset($form['rooms_fieldset']['table_body']);
				unset($form['rooms_fieldset']['rooms_label:1']);
				unset($form['rooms_fieldset']['group_size_adults:1']['#prefix']);
				unset($form['rooms_fieldset']['group_size_adults:1']['#suffix']);
				unset($form['rooms_fieldset']['group_size_children:1']['#prefix']);
				unset($form['rooms_fieldset']['group_size_children:1']['#suffix']);
				unset($form['rooms_fieldset']['table_row:1']);
				unset($form['rooms_fieldset']['table_end']);
				
			} else {
				$form['rooms_date_range']['rooms_start_date']['#prefix'] = '<div class="check_availability-field">';
				$form['rooms_date_range']['rooms_start_date']['#suffix'] = '</div>';
				$form['rooms_date_range']['rooms_end_date']['#prefix'] = '<div class="check_availability-field">';
				$form['rooms_date_range']['rooms_end_date']['#suffix'] = '</div>';
				
				$form['conditions']['rooms']['#prefix'] = '<div class="check_availability-field">';
				$form['conditions']['rooms']['#suffix'] = '</div>';
				$form['conditions']['rooms']['#attributes']['class'][] = 'awe-select';
				$form['conditions']['rooms']['#title'] = t('Rooms');
				
				$form['unit_type']['#prefix'] = '<div class="check_availability-field">';
				$form['unit_type']['#suffix'] = '</div>';
				$form['unit_type']['#attributes']['class'][] = 'awe-select';
		
				$total = $form['conditions']['rooms']['#default_value'];
				
				for($i=1;$i <= $total; $i++) {
					$form['rooms_fieldset']['rooms_label:' . $i]['#markup'] = t('Room ') . $i;
					$form['rooms_fieldset']['group_size_adults:' . $i]['#attributes']['class'][] = 'awe-select';
					$form['rooms_fieldset']['group_size_children:' . $i]['#attributes']['class'][] = 'awe-select';
				}
	
				$form['rooms_fieldset']['group_size_adults:1']['#title'] = t('Adult');
				$form['rooms_fieldset']['group_size_children:1']['#title'] = t('Child');
				
				$form['unit_type']['#attributes']['class'][] = 'awe-select';
				
				$form['actions']['#prefix'] = '<div class="check_availability-field">';
				$form['actions']['#suffix'] = '</div>';
				$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
			}
		} else {
			$form['rooms_date_range']['rooms_start_date']['#prefix'] = '<div class="check_availability-field">';
			$form['rooms_date_range']['rooms_start_date']['#suffix'] = '</div>';
			$form['rooms_date_range']['rooms_end_date']['#prefix'] = '<div class="check_availability-field">';
			$form['rooms_date_range']['rooms_end_date']['#suffix'] = '</div>';
			
			$form['conditions']['rooms']['#prefix'] = '<div class="check_availability-field">';
			$form['conditions']['rooms']['#suffix'] = '</div>';
			$form['conditions']['rooms']['#attributes']['class'][] = 'awe-select';
			$form['conditions']['rooms']['#title'] = t('Rooms');
			
			$form['unit_type']['#prefix'] = '<div class="check_availability-field">';
			$form['unit_type']['#suffix'] = '</div>';
			$form['unit_type']['#attributes']['class'][] = 'awe-select';
	
			$total = $form['conditions']['rooms']['#default_value'];
			
			for($i=1;$i <= $total; $i++) {
				$form['rooms_fieldset']['rooms_label:' . $i]['#markup'] = t('Room ') . $i;
				$form['rooms_fieldset']['group_size_adults:' . $i]['#attributes']['class'][] = 'awe-select';
				$form['rooms_fieldset']['group_size_children:' . $i]['#attributes']['class'][] = 'awe-select';
			}

			$form['rooms_fieldset']['group_size_adults:1']['#title'] = t('Adult');
			$form['rooms_fieldset']['group_size_children:1']['#title'] = t('Child');
			
			$form['unit_type']['#attributes']['class'][] = 'awe-select';
			
			$form['actions']['#prefix'] = '<div class="check_availability-field">';
			$form['actions']['#suffix'] = '</div>';
			$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
		}
	}
}

function md_hillter_form_rooms_booking_availability_search_form_page_alter(&$form, &$form_state) {
	$form['#prefix'] = '<div class="room-detail_book"><div class="room-detail_form">';
	$form['#suffix'] = '</div></div>';
	
	$form['rooms_date_range']['rooms_start_date']['#attributes']['class'][] = 'awe-calendar arrival-calendar';
	$form['rooms_date_range']['rooms_end_date']['#attributes']['class'][] = 'awe-calendar departure-calendar';
	
	$form['conditions']['rooms']['#attributes']['class'][] = 'awe-select';
	$form['conditions']['rooms']['#title'] = t('Rooms');
	
	$form['unit_type']['#attributes']['class'][] = 'awe-select';
	
	$total = $form['conditions']['rooms']['#default_value'];
	
	for($i=1;$i <= $total; $i++) {
		$form['rooms_fieldset']['rooms_label:' . $i]['#markup'] = t('Room ') . $i;
		$form['rooms_fieldset']['group_size_adults:' . $i]['#attributes']['class'][] = 'awe-select';
		$form['rooms_fieldset']['group_size_children:' . $i]['#attributes']['class'][] = 'awe-select';
	}
	
	$form['rooms_fieldset']['group_size_adults:1']['#title'] = t('Adult');
	$form['rooms_fieldset']['group_size_children:1']['#title'] = t('Child');
	
	$form['actions']['submit']['#attributes']['class'][] = 'awe-btn awe-btn-13';
	$form['actions']['submit']['#value'] = t('CHECK NOW');
}

function md_hillter_preprocess_node(&$vars) {
  if ($vars['type'] == 'unit_description') {
    $data     = _md_rooms_get_rooms_detail($vars['nid']);
    $cur      = commerce_multicurrency_get_user_currency_code();
    $cur_load = commerce_currency_load($cur);

    $price = commerce_currency_convert($data['price'], commerce_default_currency(), $cur);
    $price = round($price, 2);

    $vars['price'] = $price;
    $vars['sym']   = $cur_load['symbol'];
  }
}

function _md_rooms_get_rooms_detail($nid) {
  $rooms = rooms_unit_get_types();
  $rooms_nid = array();
  foreach ($rooms as $type => $data) {
    if (isset($data->data['rooms_description_source'])) {
      if ($data->data['rooms_description_source'] != '') {
        $source_ref = explode(':', $data->data['rooms_description_source']);
        if (isset($source_ref[1])) {
          $node_id = $source_ref[1];
          if (module_exists('translation')) {
            $node_translations = translation_node_get_translations($node_id);
            if (!empty($node_translations)) {
              $node_id = $node_translations[$GLOBALS['language']->language]->nid;
            }
          }
          $rooms_nid[$node_id]['type'] = $type;
          $rooms_nid[$node_id]['price'] = $data->data['base_price'];
        }
      }
    }
  }
  if(isset($rooms_nid[$nid])){
    return $rooms_nid[$nid];
  }

}

function md_hillter_preprocess_rooms_booking_results(&$vars) {
  if (isset($vars['units_per_type_form'])) {
    $units_per_type_form = $vars['units_per_type_form'];
    $units_per_type = _md_rooms_get_units_per_type($units_per_type_form);
	foreach ($units_per_type as $room => $room_data) {
      $price = array_keys($room_data);
      $price = $price[0];
      $id_field_start = 'rooms_' . $room . '_' . $price ;
      $id_fieldset = $room . '_' . $price . '_container';
      $units_per_type_form[$room]['open-markup']['#markup'] = '<div class="rooms-search-result__booking-form">';
      $units_per_type_form[$room]['close-markup']['#markup'] = '</div>';
      $units_per_type_form[$room][$price]['description']['#prefix'] = '';
      $units_per_type_form[$room][$price]['description']['#suffix'] = '';
      $units_per_type_form[$room][$price]['price']['#prefix'] = '<div class="rooms-search-result__unit_summary_wrapper"><div class="rooms-search-result__base_price hidden">';
      $units_per_type_form[$room][$price]['price_value']['#suffix'] = '</div>';
      $units_per_type_form[$room][$price]['quantity']['#prefix'] = '<div class="rooms-search-result__select-units">';
      $units_per_type_form[$room][$price]['quantity']['#suffix'] = '</div>';
	  $units_per_type_form[$room][$price]['quantity']['#attributes']['class'][] = 'awe-select';
      $units_per_type_form[$room][$price]['submit']['#prefix'] = '<div class="rooms-search-result__booking-legend"><div class="booking-legend booking-legend--rooms-search-result">';
      $units_per_type_form[$room][$price]['submit']['#suffix'] = '</div></div></div></div></div></div></div>';
	  $units_per_type_form[$room][$price]['submit']['#attributes']['class'][] = 'awe-btn awe-btn-default';
      $units_per_type_form[$room][$price]['field_start']['#markup'] = '<div id="' . $id_field_start . '" style="display:none;"><div class="rooms-search-result__unit_options_wrapper" colspan="2">';
      $units_per_type_form[$room][$price]['fieldset']['#prefix'] = '<div class="rooms-search-result__unit_options form-wrapper" id="' . $id_fieldset . '">';
      $units_per_type_form[$room][$price]['fieldset']['#suffix'] = ' </div>';
      $units_per_type_form[$room][$price]['close-row']['#markup'] = ' </div></div>';
      $units_per_type_form[$room]['close-markup']['#markup'] = '</div>';
    }
    $vars['units_per_type_form'] = $units_per_type_form;
  }
}

function _md_rooms_get_units_per_type($units) {
  $booking_parameters = rooms_booking_manager_retrieve_booking_parameters(1, $_GET);
  $type = '';
  $start_date = $units['start_date']['#value'];
  $end_date = $units['end_date']['#value'];
  $unit_types = array();
  if (isset($_GET['type'])) {
    $type = check_plain($_GET['type']);

    if ($_GET['type'] == 'all') {
      $types = variable_get('rooms_unit_type_selector', array());

      foreach ($types as $element) {
        $unit_types[] = $element;
      }
    }
    else {
      $type = check_plain($_GET['type']);

      if (rooms_unit_type_load($type) !== FALSE) {
        $unit_types[] = $type;
      }
    }
  }
  $agent = new AvailabilityAgent($start_date, $end_date, $booking_parameters, 1, array_keys(array_filter(variable_get('rooms_valid_availability_states', drupal_map_assoc(array(ROOMS_AVAILABLE, ROOMS_ON_REQUEST))))), $unit_types);
  $units_per_type = $agent->checkAvailability();

  return $units_per_type;
}

function md_hillter_form_rooms_booking_manager_change_search_form_alter(&$form, &$form_state) {
	$form['info']['actions']['change_search']['#attributes']['class'][] = 'awe-btn awe-btn-13';
}
function md_hillter_form_book_units_per_type_form_alter(&$form, &$form_state, $id) {
  $form['actions']['place_booking']['#attributes']['class'][] = 'awe-btn awe-btn-13';
  // Recive room types from form_state
  $room_types = $form_state['build_info']['args'][0];
  foreach ($room_types as $room_type => $type) {
    if (!isset($form[$room_type])) {
      continue;
    }
    foreach ($form[$room_type] as $price => &$data) {
      if (!isset($data['fieldset'])) {
        continue;
      }
      
      foreach ($data['fieldset'] as &$c) {
        if (is_array($c) && isset($c['price'])) {
          $cur      = commerce_multicurrency_get_user_currency_code();
          $cur_load = commerce_currency_load($cur);

          list (, $price) = explode(" ", $c['price']['#markup']);
          
          $price = commerce_currency_convert($price, commerce_default_currency(), $cur);
          $price = round($price, 2);
          $c['price']['#markup'] = $cur_load['symbol'] . ' ' . $price;
        }
      }
    }
  }
}
function md_hillter_preprocess_block(&$vars) {
  	if($vars['block_html_id'] == "block-locale-language") {
		$vars['content'] = str_replace('xml:lang="en"', '', $vars['content']);
		$vars['content'] = str_replace('xml:lang="fr"', '', $vars['content']);
	}
}
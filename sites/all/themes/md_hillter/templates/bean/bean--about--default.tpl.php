<div class="about">

  <!-- ITEM -->
  <div class="about-item <?php if($content['field_about_align']['#items'][0]['value'] == "left") print "about-right"; ?>">

      <?php if(isset($content['field_about_images'])) : ?>
		  <?php
              if(count($content['field_about_images']['#items']) > 1) :
          ?>
                  <div class="img owl-single">
                      <?php for($i=0; $i < count($content['field_about_images']['#items']); $i++) : ?>
                          <img src="<?php print file_create_url($content['field_about_images']['#items'][$i]['uri']); ?>" alt="">
                      <?php endfor; ?>
                  </div>
          <?php
              else :
          ?>
                  <div class="img">
                      <img src="<?php print file_create_url($content['field_about_images']['#items'][0]['uri']); ?>" alt="">
                  </div>
          <?php
              endif;
          ?>
      <?php endif; ?>
      
      <?php if(isset($content['field_about_media'])) : ?>
		  <div class="img">
              <?php print render($content['field_about_media']); ?>
          </div>
      <?php endif; ?>

      <div class="text">
          <h2 class="heading"><?php print $content['field_about_title']['#items'][0]['value']; ?></h2>
          <div class="desc">
              <?php print $content['field_about_description']['#items'][0]['value']; ?>
          </div>
      </div>

  </div>
  <!-- END / ITEM -->

</div>
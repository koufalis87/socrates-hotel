<div class="event-slide owl-single">
    <?php for($i=0; $i < count($content['field_event_item']['#items']); $i++) : ?>
        <div class="event-item">
            <div class="img">
                <a href="<?php print $content['field_event_item'][$i]['entity']['field_collection_item'][$content['field_event_item']['#items'][$i]['value']]['field_event_item_link']['#items'][0]['value']; ?>">
                    <img src="<?php print file_create_url($content['field_event_item'][$i]['entity']['field_collection_item'][$content['field_event_item']['#items'][$i]['value']]['field_event_item_image']['#items'][0]['uri']) ?>" alt="">
                </a>
            </div>
        </div>
    <?php endfor; ?>
</div>
<?php $coord = ""; ?>
<?php for($i=0;$i < count($content['field_gm_coordinates']['#items']); $i++) : ?>
	<?php if($i < count($content['field_gm_coordinates']['#items']) - 1) : ?>
		<?php $coord .= $content['field_gm_coordinates']['#items'][$i]['value'] . "--"; ?>
    <?php else: ?>
    	<?php $coord .= $content['field_gm_coordinates']['#items'][$i]['value']; ?>
    <?php endif; ?>
<?php endfor; ?>
<!-- MAP -->
<section class="section-map">
    <div class="contact-map">
        <div id="map" data-locations="<?php print $coord; ?>" data-center="<?php print $content['field_gm_coordinates_center']['#items'][0]['value'] ?>" data-marker="<?php print file_create_url($content['field_gm_icon_marker']['#items'][0]['uri']); ?>"></div>
    </div>
</section>
<!-- END / MAP -->
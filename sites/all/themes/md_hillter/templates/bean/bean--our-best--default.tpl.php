<div class="our-best">
    <div class="row">

        <?php if($content['field_best_media_align']['#items'][0]['value'] == "right") : ?>
            <div class="col-md-6 col-md-push-6">
                <div class="img">
                    <?php if($content['field_best_multimedia'][0]['file']['#theme'] == "image_formatter") : ?>
                    	<img src="<?php print file_create_url($content['field_best_multimedia'][0]['file']['#item']['uri']); ?>"alt="">
                    <?php else : ?>
                    	<?php
							if(strpos($content['field_best_multimedia'][0]['file']['#uri'], 'vimeo') !== FALSE) :
								$uri = explode('v/',$content['field_best_multimedia'][0]['file']['#uri']);
								print '<iframe src="http://player.vimeo.com/video/'. $uri[1].'?title=0&amp;byline=0&amp;portrait=0&amp;color=ec155a" height="460"></iframe>';
							else :
								$uri = explode('v/',$content['field_best_multimedia'][0]['file']['#uri']);
								print '<iframe src="http://www.youtube.com/embed/'. $uri[1].'?feature=oembed" height="460"></iframe>';
							endif;
						?>
					<?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="col-md-6 <?php ($content['field_best_media_align']['#items'][0]['value'] == "right") ? print "col-md-pull-6" : print "col-md-push-6"; ?>">
            <div class="text">
                <h2 class="heading"><?php print $title; ?></h2>
                <p><?php print $content['field_best_description']['#items'][0]['value']; ?></p>

                <ul>
                    <?php for($i=0; $i < count($content['field_best_features']['#items']); $i++) : ?>
                        <li><?php print $content['field_best_features'][$i]['entity']['field_collection_item'][$content['field_best_features']['#items'][$i]['value']]['field_best_features_text']['#items'][0]['value'] ?></li>
                    <?php endfor; ?>
                </ul>
                
            </div>
        </div>
        
        <?php if($content['field_best_media_align']['#items'][0]['value'] == "left") : ?>
            <div class="col-md-6 col-md-pull-6">
                <div class="img">
                    <?php if($content['field_best_multimedia'][0]['file']['#theme'] == "image_formatter") : ?>
                    	<img src="<?php print file_create_url($content['field_best_multimedia'][0]['file']['#item']['uri']); ?>"alt="">
                    <?php else : ?>
                    	<?php
							if(strpos($content['field_best_multimedia'][0]['file']['#uri'], 'vimeo') !== FALSE) :
								$uri = explode('v/',$content['field_best_multimedia'][0]['file']['#uri']);
								print '<iframe src="http://player.vimeo.com/video/'. $uri[1].'?title=0&amp;byline=0&amp;portrait=0&amp;color=ec155a" height="460"></iframe>';
							else :
								$uri = explode('v/',$content['field_best_multimedia'][0]['file']['#uri']);
								print '<iframe src="http://www.youtube.com/embed/'. $uri[1].'?feature=oembed" height="460"></iframe>';
							endif;
						?>
					<?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>
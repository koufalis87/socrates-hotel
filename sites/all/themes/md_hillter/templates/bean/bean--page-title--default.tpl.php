<!-- SUB BANNER -->
<section class="section-sub-banner awe-parallax bg-3" style="background-image:url(<?php print file_create_url($content['field_pt_background_image']['#items'][0]['uri']); ?>);">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php print drupal_get_title(); ?></h2>
            </div>
        </div>

    </div>
</section>
<!-- END / SUB BANNER -->
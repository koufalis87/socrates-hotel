<div class="banner-slider" id="banner-slider">
	<?php for($i=0; $i < count($content['field_slider_item']['#items']); $i++) : ?>
    	<!-- ITEM -->
        <div class="slider-item text-center" data-image="<?php print file_create_url($content['field_slider_item'][$i]['entity']['field_collection_item'][$content['field_slider_item']['#items'][$i]['value']]['field_slider_item_image']['#items'][0]['uri']); ?>">
            <div class="slider-text">
                <?php print $content['field_slider_item'][$i]['entity']['field_collection_item'][$content['field_slider_item']['#items'][$i]['value']]['field_slider_item_text']['#items'][0]['value']; ?>
            </div>
        </div>
        <!-- ITEM -->
	<?php endfor; ?>
</div>
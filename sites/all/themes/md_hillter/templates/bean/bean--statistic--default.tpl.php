<div class="statistics_content">
    <div class="row">

        <?php for($i=0; $i < count($content['field_statistic_item']['#items']); $i++) : ?>
        	<!-- ITEM -->
            <div class="col-xs-6 col-md-3">
                <div class="statistics_item">
                    <span class="count"><?php print $content['field_statistic_item'][$i]['entity']['field_collection_item'][$content['field_statistic_item']['#items'][$i]['value']]['field_statistic_number']['#items'][0]['value'] ?></span>
                    <span><?php print $content['field_statistic_item'][$i]['entity']['field_collection_item'][$content['field_statistic_item']['#items'][$i]['value']]['field_statistic_title']['#items'][0]['value'] ?></span>
                </div>
            </div>
            <!-- END ITEM -->
		<?php endfor; ?>

    </div>
</div>
<div class="col-xs-12 col-sm-12 <?php print $classes; ?>" <?php print $attributes; ?>>
	<?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
        <h3 class="title-block"><?php print $block->subject;?></h3>
    <?php endif;?>
    <?php print render($title_suffix); ?>
    <?php print $content ;?>
</div>
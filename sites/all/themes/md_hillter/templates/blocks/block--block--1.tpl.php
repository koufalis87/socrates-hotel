<div class="widget widget_social <?php print $classes; ?>" <?php print $attributes; ?> id="<?php print $block_html_id; ?>">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
    <h4 class="widget-title"><?php print $block->subject; ?></h4>
    <?php endif;?>
    <?php print render($title_suffix); ?>
    <div class="widget-social">
		<?php print render($content); ?>
    </div>
</div>
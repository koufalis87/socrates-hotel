<div class="col-xs-4 col-lg-3 <?php print $classes; ?>" <?php print $attributes; ?> id="<?php print $block_html_id; ?>">
    <div class="widget widget_tripadvisor">
		<?php print render($title_prefix); ?>
        <?php if ($block->subject): ?>
            <h4 class="widget-title"><?php print $block->subject; ?></h4>
        <?php endif;?>
        <?php print render($title_suffix); ?>
        <?php print render($content); ?>
    </div>
</div>
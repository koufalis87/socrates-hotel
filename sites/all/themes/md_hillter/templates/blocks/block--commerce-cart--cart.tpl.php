<?php
global $user;
$quantity = 0;
$order = commerce_cart_order_load($user->uid);
if ($order) :
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $line_items = $wrapper->commerce_line_items;
    $quantity = commerce_line_items_quantity($line_items, commerce_product_line_item_types());
    $total = commerce_line_items_total($line_items);
    $currency = commerce_currency_load($total['currency_code']);
endif;
?>
<!-- HEADER CART MINI -->
<div class="header_cart <?php print $classes; ?>" <?php print $attributes; ?> id="<?php print $block_html_id; ?>">

    <!-- HEAD CART -->
    <div class="cart-head">
        <span class="hillter-icon-cart"></span>
    </div>
    <!-- END / HEAD CART -->
    
    <!-- CONTENT CART -->
    <div class="cart-content">

        <?php print render($title_prefix); ?>
		<?php if ($block->subject): ?>
        	<h4><?php print $block->subject; ?></h4>
        <?php endif;?>
        <?php print render($title_suffix); ?>

        <?php print $content ;?>

        <?php if($quantity > 0): ?>
            <!-- CART TOTAL -->
            <div class="cart-total">
               	<?php print t('TOTAL'); ?>
               	<span><?php print commerce_currency_format($order->commerce_order_total['und'][0]['amount'], $order->commerce_order_total['und'][0]['currency_code']); ?></span>
            </div>
            <!-- END / CART TOTAL -->
    
            <!-- CART BUTTON -->
            <div class="cart-button">
                <a href="<?php print base_path() ?>bookings" class="awe-btn"><?php print t('Reservation'); ?></a>
                <a href="<?php print base_path() ?>checkout" class="awe-btn btn-bacground"><?php print t('Check out'); ?></a>
            </div>
            <!-- END / CART BUTTON -->
        <?php endif; ?>

    </div>
    <!-- END / CONTENT CART -->
    
</div>
<!-- END HEADER CART MINI -->
<div class="dropdown language <?php print $classes; ?>" <?php print $attributes; ?> id="<?php print $block_html_id; ?>">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
    	<span><?php print $block->subject; ?> <i class="fa fa"></i></span>
    <?php endif;?>
    <?php print render($title_suffix); ?>
    <?php print render($content); ?>
</div>
<div id="<?php print $block_html_id; ?>" class="col-xs-12 col-lg-5 <?php print $classes; ?>"<?php print $attributes; ?>>
	<?php print render($title_prefix); ?>
	<?php if ($block->subject): ?>
        <h4><?php print $block->subject;?></h4>
    <?php endif;?>
    <?php print render($title_suffix); ?>
    <?php print $content ;?>
</div>
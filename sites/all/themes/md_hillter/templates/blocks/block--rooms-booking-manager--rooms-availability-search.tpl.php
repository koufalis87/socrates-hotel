<!-- WIDGET CHECK AVAILABILITY -->
<div class="widget widget_check_availability <?php print $classes; ?>" <?php print $attributes; ?> id="<?php print $block_html_id; ?>">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
    <h4 class="widget-title"><?php print $block->subject; ?></h4>
    <?php endif;?>
    <?php print render($title_suffix); ?>
    <div class="check_availability">
		<?php print render($content); ?>
    </div>
</div>
<!-- END / WIDGET CHECK AVAILABILITY -->
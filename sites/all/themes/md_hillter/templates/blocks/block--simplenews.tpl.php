<div class="col-lg-8 <?php print $classes; ?>" <?php print $attributes; ?> id="<?php print $block_html_id; ?>">
	<div class="mailchimp">
		<?php print render($title_prefix); ?>
        <?php if ($block->subject): ?>
        <h4><?php print $block->subject; ?></h4>
        <?php endif;?>
        <?php print render($title_suffix); ?>
        <div class="mailchimp-form">
			<?php print render($content); ?>
        </div>
    </div>
</div>
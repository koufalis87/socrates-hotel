<div class="room-detail_compare <?php print $classes; ?>" <?php print $attributes; ?> id="<?php print $block_html_id; ?>">
	<?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
    <h2 class="room-compare_title"><?php print $block->subject; ?></h2>
    <?php endif;?>
    <?php print render($title_suffix); ?>
    <?php print render($content); ?>
</div>
<?php if($node->comment_count > 0) : ?>
<!-- COMMENT -->
<div id="comments">
    <h4 class="comment-title"><?php print t('COMMENT'); ?> (<?php print $node->comment_count; ?>)</h4>
    <?php print render($content['comments']); ?>
</div>
<!-- END / COMMENT -->
<?php endif; ?>

<!-- COMMENT RESPOND -->
<div class="comment-respond">
    <h3 class="comment-reply-title"><?php print t('LEAVE A COMMENT'); ?></h3>
    <?php print render($content['comment_form']); ?>
</div>
<!-- END COMMENT RESPOND -->
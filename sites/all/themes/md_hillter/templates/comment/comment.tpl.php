<ul class="commentlist">
  <li>
      <div class="comment-body">

          <?php if (isset($comment->picture->uri)) : ?>
              <a class="comment-avatar"><img src="<?php print file_create_url($comment->picture->uri); ?>" alt="<?php print $content['comment_body']['#object']->name; ?>"/></a>
          <?php else : ?>
              <a class="comment-avatar"><img src="<?php print base_path().path_to_theme().'/images/no-avatar.jpg'; ?>"/></a>
          <?php endif; ?>

          <?php if(isset($comment->subject)) : ?>
          	<h4 class="comment-subject"><?php print $comment->subject; ?></h4>
          <?php endif; ?>
          <?php
			hide($content['links']);
			hide($content['field_comment_email']);
			print '<p>' . $content['comment_body']['#object']->comment_body['und'][0]['value'] . '</p>';
		  ?>

          <span class="comment-meta">
              <a><?php print $content['comment_body']['#object']->name; ?></a> <?php print format_date($comment->created, 'custom', 'F d, Y'); ?> <?php print t('at'); ?> <?php print format_date($comment->created, 'custom', 'h:i A.'); ?>
          </span>

          <div class="action">
              <a class="awe-btn awe-btn-14" href="<?php print url($content['links']['comment']['#links']['comment-reply']['href']); ?>"><?php print t('Reply'); ?></a>
          </div>

      </div>
  </li>
</ul>
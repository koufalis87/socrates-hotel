<!-- MAPS -->
<div class="attraction-maps" id="attraction-maps"></div>
<!-- END / MAPS -->

<div class="container">
    <div class="attraction">
        <div class="row">
            <div class="col-md-4">
                <div class="attraction_sidebar">
                    <?php
					  $block = module_invoke('views', 'block_view', 'attractions-attraction_list');
					  print render($block['content']);
					?>
                </div> 
            </div>

            <div class="col-md-8">

                <div class="attraction_detail">
                    <div class="attraction_detail-header">
                        <h2 class="attraction_detail-title"><i class="fa fa-map-marker"></i><?php print $node->title ?></h2>
                        <ul>
                            <li>
                                <span><?php print t('ADDRESS'); ?>:</span> <?php print $content['field_att_address']['#items'][0]['value']; ?><br>
                            </li>
                            <li>
                                <span><?php print t('Website'); ?>:</span> <a href="<?php print $content['field_att_website']['#items'][0]['value'] ?>"><?php print $node->title ?></a><br>
                            </li>
                            <li>
                                <span><?php print t('Google map'); ?>:</span> <a href="<?php print $content['field_att_google_map']['#items'][0]['value'] ?>"><?php print t('Google Map'); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="attraction_content" id="attraction_content">

                    <?php print render($content['body']); ?>

                </div>

            </div>
        </div>    

    </div>

</div>
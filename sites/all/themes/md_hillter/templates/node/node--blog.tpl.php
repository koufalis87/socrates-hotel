<!-- POST SINGLE -->
<article class="post post-single">

    <div class="entry-media">
        <?php if(isset($content['field_blog_multimedia'])) : ?>
			<?php print render($content['field_blog_multimedia']); ?>    
        <?php else : ?>
            <div class="post-slider owl-single">
				<?php for($i=0; $i < count($content['field_blog_images']['#items']); $i++) : ?>
                    <img src="<?php print file_create_url($content['field_blog_images'][$i]['#item']['uri']); ?>" alt="">
                <?php endfor; ?>
            </div>
        <?php endif; ?>
        <span class="posted-on"><strong><?php print format_date($node->created, 'custom', 'd'); ?></strong><?php print format_date($node->created, 'custom', 'M'); ?></span>
    </div>
    
    <div class="entry-header">

        <h1 class="entry-title"><?php print $node->title; ?></h1>

        <p class="entry-meta">

            <span class="entry-author">
                <span class="screen-reader-text"><?php print t('Posted by'); ?> </span>
                <a href="<?php print base_path() . 'user/' . $node->uid; ?>" class="entry-author-link">
                    <span class="entry-author-name"><?php print $node->name; ?></span>
                </a>
            </span>

            <span class="entry-categories">
                <?php for($i=0; $i < count($content['field_blog_categories']['#items']); $i++) : ?>
                	<?php if($i < count($content['field_blog_categories']['#items'])-1) : ?>
                    	<a href="<?php print $content['field_blog_categories'][$i]['#href']; ?>"><?php print $content['field_blog_categories'][$i]['#title']; ?></a>,
					<?php else : ?>
                    	<a href="<?php print $content['field_blog_categories'][$i]['#href']; ?>"><?php print $content['field_blog_categories'][$i]['#title']; ?></a>
                    <?php endif; ?>
				<?php endfor; ?>
            </span>

            <span class="entry-comments-link">
                <a href="#"><?php print $node->comment_count; ?> <?php print t('Comments'); ?></a>
            </span>
        </p>

    </div>

    <div class="entry-content">
		<?php print render($content['body']); ?>
    </div>

</article>
<!-- END / POST SINGLE -->

<?php print render($content['comments']); ?>
<?php if ($view_mode == 'rooms_list'): ?>
	<div class="reservation_content">
                                
      <!-- RESERVATION ROOM -->
      <div class="reservation-room">
          <!-- ITEM -->
          <div class="reservation-room_item">
      
              <h2 class="reservation-room_name"><a href="<?php print base_path().drupal_get_path_alias('node/' . $node->nid); ?>"><?php print $node->title; ?></a></h2>
      
              <div class="reservation-room_img">
                  <a href="<?php print base_path().drupal_get_path_alias('node/' . $node->nid); ?>"><img src="<?php print image_style_url('rooms_v9', $content['field_unit_thumbnail']['#items'][0]['uri']) ?>" alt=""></a>
              </div>
      
              <div class="reservation-room_text">
      
                  <div class="reservation-room_desc">
                      <?php print $content['field_unit_description']['#items'][0]['value']; ?>
                      <ul>
                          <li><?php print t('Bed') ?>: <?php print $content['field_unit_bed']['#items'][0]['value']; ?></li>
                          <li><?php print t('Size') ?>: <?php print $content['field_unit_size_square']['#items'][0]['value']; ?></li>
                          <li><?php print t('View') ?>: <?php print $content['field_unit_view']['#items'][0]['value']; ?></li>
                           
                      </ul>
                  </div>
      
                  <a href="<?php print base_path().drupal_get_path_alias('node/' . $node->nid); ?>" class="reservation-room_view-more"><?php print t('View More Information'); ?></a>
      
                  <div class="clear"></div>
      
                  <p class="reservation-room_price">
                      <span class="reservation-room_amout"><?php print $sym.$price; ?></span> / <?php print t('night(s)'); ?>
                  </p>
              
          
<?php else : ?>
	<!-- DETAIL -->
    <div class="room-detail">
        <div class="row">
            <div class="col-lg-9">
                
                <!-- LAGER IMGAE -->
                <div class="room-detail_img">
                  <?php for($i=0; $i < count($content['field_unit_gallery']['#items']); $i++) : ?>
                    <div class="room_img-item">
                        <img src="<?php print image_style_url('gallery', $content['field_unit_gallery'][$i]['entity']['field_collection_item'][$content['field_unit_gallery']['#items'][$i]['value']]['field_unit_gallery_large_image'][0]['#item']['uri']); ?>" alt="">    
                        <h6><?php print $content['field_unit_gallery'][$i]['entity']['field_collection_item'][$content['field_unit_gallery']['#items'][$i]['value']]['field_unit_gallery_description']['#items'][0]['value'] ?></h6>
                    </div>	
                  <?php endfor; ?>
                </div>
                <!-- END / LAGER IMGAE -->
                
                <!-- THUMBNAIL IMAGE -->
                <div class="room-detail_thumbs">
                  <?php for($i=0; $i < count($content['field_unit_gallery']['#items']); $i++) : ?>
                    <a href="javascript:void(0)"><img src="<?php print image_style_url('gallery_thumb', $content['field_unit_gallery'][$i]['entity']['field_collection_item'][$content['field_unit_gallery']['#items'][$i]['value']]['field_unit_gallery_thumb_image'][0]['#item']['uri']); ?>" alt=""></a>
                  <?php endfor; ?>
                </div>
                <!-- END / THUMBNAIL IMAGE -->
    
            </div>
    
            <div class="col-lg-3">
    
                <!-- FORM BOOK -->
                <div class="room-detail_book">
    
                    <div class="room-detail_total">
                        <img src="<?php print base_path().path_to_theme() ?>/images/icon-logo.png" alt="" class="icon-logo">
                        
                        <h6><?php print t('STARTING ROOM FROM'); ?></h6>
                        
                        <p class="price">
                            <span class="amout"><?php print $sym.$price; ?></span>  /<?php print t('night'); ?>
                        </p>
                    </div>
                    
                    <div class="room-detail_form">
                        <?php
                            module_load_include('inc', 'rooms_booking_manager', 'rooms_booking_manager.availability_search');
                            $form = drupal_get_form('rooms_booking_availability_search_form_block');
                            print render($form);
                        ?>
                    </div>
                </div>
                <!-- END / FORM BOOK -->
    
            </div>
        </div>
    </div>
    <!-- END / DETAIL -->
    
    <!-- TAB -->
    <div class="room-detail_tab">
        
        <div class="row">
            <div class="col-md-3">
                <ul class="room-detail_tab-header">
                    <li class="active"><a href="#overview" data-toggle="tab"><?php print t('Overview'); ?></a></li>
                    <li><a href="#amenities" data-toggle="tab"><?php print t('Amenities'); ?></a></li>
                    <li><a href="#rates" data-toggle="tab"><?php print t('Rates'); ?></a></li>
                    <li><a href="#calendar" data-toggle="tab"><?php print t('Calendar'); ?></a></li>
                </ul>
            </div>
                            
            <div class="col-md-9">
                <div class="room-detail_tab-content tab-content">
                    
                    <!-- OVERVIEW -->
                    <div class="tab-pane fade active in" id="overview">
    
                        <div class="room-detail_overview">
                            <p><?php print $content['field_unit_description']['#items'][0]['value']; ?></p>
    
                            <div class="row">
                                <div class="col-xs-6 col-md-4">
                                    <h6><?php print t('SPECIAL ROOM'); ?></h6>
                                    <ul>
                                      <li><?php print t('Max'); ?>: <?php print $content['field_unit_max_person']['#items'][0]['value']; ?> <?php print t('Person(s)'); ?></li>
                                      <li><?php print t('Size'); ?>: <?php print $content['field_unit_size_square']['#items'][0]['value'] ?> m2 / <?php print $content['field_unit_size_ft']['#items'][0]['value'] ?> ft2</li>
                                      <li><?php print t('View'); ?>: <?php print $content['field_unit_view']['#items'][0]['value']; ?></li>
                                      <li><?php print t('Bed'); ?>: <?php print $content['field_unit_bed']['#items'][0]['value']; ?></li>
                                    </ul>
                                </div>
                                <div class="col-xs-6 col-md-4">
                                    <h6><?php print t('SERVICE ROOM'); ?></h6>
                                    <ul>
                                      <?php for($i=0; $i < count($content['field_unit_services']['#items']); $i++) : ?>
                                        <li><?php print $content['field_unit_services']['#items'][$i]['value']; ?></li>
                                      <?php endfor; ?>
                                    </ul>
                                </div>
                            </div>
    
                        </div>
    
                    </div>
                    <!-- END / OVERVIEW -->
    
                    <!-- AMENITIES -->
                    <div class="tab-pane fade" id="amenities">
                        
                        <div class="room-detail_amenities">
                            <p><?php print $content['field_unit_amenities_des']['#items'][0]['value']; ?></p>
                            
                            <div class="row">
                              <?php for($i=0; $i < count($content['field_unit_amenities_item']['#items']); $i++) : ?>
                                <div class="col-xs-6 col-md-4">
                                    <h6><?php print $content['field_unit_amenities_item'][$i]['entity']['field_collection_item'][$content['field_unit_amenities_item']['#items'][$i]['value']]['field_unit_amenities_item_title']['#items'][0]['value']; ?></h6>
                                    <ul>
                                        <?php for($j=0; $j < count($content['field_unit_amenities_item'][$i]['entity']['field_collection_item'][$content['field_unit_amenities_item']['#items'][$i]['value']]['field_unit_amenities_features']['#items']); $j++) : ?>
                                            <li><?php print $content['field_unit_amenities_item'][$i]['entity']['field_collection_item'][$content['field_unit_amenities_item']['#items'][$i]['value']]['field_unit_amenities_features']['#items'][$j]['value'] ?></li>
                                        <?php endfor; ?>
                                    </ul>
                                </div>
                              <?php endfor; ?>
                            </div>
    
                        </div>
    
                    </div>
                    <!-- END / AMENITIES -->
    
                    <!-- RATES -->
                    <div class="tab-pane fade" id="rates">
    
                        <div class="room-detail_rates">
                            <?php print $content['field_unit_rate_info']['#items'][0]['value']; ?>
                        </div>
    
                    </div>
                    <!-- END / RATES -->
    
                    <!-- CALENDAR -->
                    <div class="tab-pane fade" id="calendar">
    
                      <div class="room-detail_calendar-wrap row">
    
                          <?php
                            $calendar = render($content['field_unit_check_available']);
                            $calendar = str_replace('<div>', '<a href="#"><small>', $calendar);
                            $calendar = str_replace('</div></td>', '</a></td>', $calendar);
                            $calendar = str_replace("cal-month", "col-sm-12", $calendar);
                            $calendar = str_replace("<table>", '<div class="calendar_custom"><table class="calendar_tabel">', $calendar);
                            $calendar = str_replace("</table>", '</table></div>', $calendar);
                            $calendar = str_replace('<tr><td class="cal-empty" colspan="7"><a href="#"><small></small></a></td> </tr>', '', $calendar);
                            $calendar = str_replace('cal-1 cal', '', $calendar);
                            $calendar = str_replace('cal-nc', 'not-available', $calendar);
                            $calendar = str_replace('<caption>', '<caption class="calendar_title">', $calendar);
                            $calendar = str_replace('cal-pastdate', 'not-available', $calendar);
                          ?>
                          <?php print $calendar; ?>
                          
                          <div class="calendar_status text-center col-sm-12">
                              <span><?php print t('Available'); ?></span>
                              <span class="not-available"><?php print t('Not Available'); ?></span>
                          </div>
                      </div>
    
                    </div>
                    <!-- END / CALENDAR -->
    
                </div>
            </div>
    
        </div>
    
    </div>
    <!-- END / TAB -->
<?php endif; ?>
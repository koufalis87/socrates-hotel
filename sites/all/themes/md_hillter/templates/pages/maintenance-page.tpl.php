<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php if(isset($ios_144) && $ios_144 != null) :?><link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php print $ios_144; ?>"><?php endif;?>
    <?php if(isset($ios_114) && $ios_114 != null) :?><link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php print $ios_114; ?>"><?php endif;?>
    <?php if(isset($ios_72) && $ios_72 != null)  :?><link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php print $ios_72; ?>"><?php endif;?>
    <?php if(isset($ios_57) && $ios_57 != null)  :?><link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php print $ios_57; ?>"><?php endif;?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php
        print $styles;
        print $scripts;
        global $base_url;
    ?>
    <style type="text/css">
        <?php if (isset($googlewebfonts)): print $googlewebfonts; endif; ?>
        <?php if (isset($theme_setting_css)): print $theme_setting_css; endif; ?>
        <?php
        // custom typography
        if (isset($typography)): print $typography; endif;
        ?>
        <?php if (isset($custom_css)): print $custom_css; endif; ?>
    </style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
	<!-- PRELOADER -->
<div id="preloader">
    <span class="preloader-dot"></span>
</div>
<!-- END / PRELOADER -->

<!-- PAGE WRAP -->
<div id="page-wrap">
    
    <?php print $messages; ?>
    <?php
		$default_value = theme_get_setting('comming_image_file_uploaded');
		$media_file = array('fid' => isset($default_value['fid']) ? intval($default_value['fid']) : 0);
		if ($media_file['fid'] && ($media_file = file_load($media_file['fid']))) {
		  $media_file = get_object_vars($media_file);
		}
	?>
    <section class="section-comingsoon bg-6" style="background-image:url(<?php print file_create_url($media_file['uri']); ?>);">
        <div class="awe-overlay"></div>
        <div class="comingsoon text-center">
            <a href="<?php print base_path(); ?>"><img src="<?php print $logo; ?>" alt=""></a>
            <h1><?php print theme_get_setting('comming_title'); ?></h1>
            <?php print theme_get_setting('comming_subtitle'); ?>

            <div class="countdown">
                <div id="countdown"
                	 year-data = "<?php print theme_get_setting('comming_year'); ?>"
                     month-data = "<?php print theme_get_setting('comming_month'); ?>"
                     day-data = "<?php print theme_get_setting('comming_day'); ?>">
                </div>
            </div>

            <div class="widget widget_social">
                <h4 class="widget-title"><?php print theme_get_setting('comming_follow'); ?></h4>
                <div class="widget-social">
                    <?php
					  $block = module_invoke('md_theme', 'block_view', 'footer');
					  print render($block['content']);
					?>
                </div>
            </div>
            
        </div>

    </section>

</div>
<!-- END / PAGE WRAP -->
</body>

</html>
<!-- PRELOADER -->
<div id="preloader">
    <span class="preloader-dot"></span>
</div>
<!-- END / PRELOADER -->

<!-- PAGE WRAP -->
<div id="page-wrap">
    
    <?php
		$default_value = theme_get_setting('comming_image_file_uploaded');
		$media_file = array('fid' => isset($default_value['fid']) ? intval($default_value['fid']) : 0);
		if ($media_file['fid'] && ($media_file = file_load($media_file['fid']))) {
		  $media_file = get_object_vars($media_file);
		}
	?>
    <section class="section-comingsoon bg-6" style="background-image:url(<?php print file_create_url($media_file['uri']); ?>);">
        <div class="awe-overlay"></div>
        <div class="comingsoon text-center">
            <a href="<?php print base_path(); ?>"><img src="<?php print $logo; ?>" alt=""></a>
            <h1><?php print theme_get_setting('comming_title'); ?></h1>
            <?php print theme_get_setting('comming_subtitle'); ?>

            <div class="countdown">
                <div id="countdown"
                	 year-data = "<?php print theme_get_setting('comming_year'); ?>"
                     month-data = "<?php print theme_get_setting('comming_month'); ?>"
                     day-data = "<?php print theme_get_setting('comming_day'); ?>">
                </div>
            </div>

            <div class="widget widget_social">
                <h4 class="widget-title"><?php print theme_get_setting('comming_follow'); ?></h4>
                <div class="widget-social">
                    <?php
					  $block = module_invoke('md_theme', 'block_view', 'footer');
					  print render($block['content']);
					?>
                </div>
            </div>
            
        </div>

    </section>

</div>
<!-- END / PAGE WRAP -->
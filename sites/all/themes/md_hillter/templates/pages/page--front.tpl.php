<!-- PRELOADER -->
<div id="preloader">
    <span class="preloader-dot"></span>
</div>
<!-- END / PRELOADER -->

<!-- PAGE WRAP -->
<div id="page-wrap">

    <!-- HEADER -->
    <header id="header">
        
        <!-- HEADER TOP -->
        <div class="header_top">
            <div class="container">
                <?php if(theme_get_setting('header_info')) : ?>
                    <div class="header_left float-left">
                        <?php print theme_get_setting('header_info'); ?>
                    </div>
                <?php endif; ?>
                <div class="header_right float-right">
					<?php if ($page['top']) : ?>
						<?php print render($page['top']); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- END / HEADER TOP -->
        
        <!-- HEADER LOGO & MENU -->
        <div class="header_content" id="header_content">

            <div class="container">
                <!-- HEADER LOGO -->
                <div class="header_logo">
                    <a href="<?php print base_path(); ?>"><img src="<?php print $logo; ?>" alt=""></a>
                </div>
                <!-- END / HEADER LOGO -->
                
                <?php if ($page['menu']) : ?>
					<?php print render($page['menu']); ?>
                <?php endif; ?>

                <!-- MENU BAR -->
                <span class="menu-bars">
                    <span></span>
                </span>
                <!-- END / MENU BAR -->

            </div>
        </div>
        <!-- END / HEADER LOGO & MENU -->

    </header>
    <!-- END / HEADER -->

    <?php if ($page['banner']) : ?>
		<?php print render($page['banner']); ?>
    <?php endif; ?>

    <?php print $messages; ?>
	<?php print render($page['content']); ?>
    
    <?php if ($page['footer'] || $page['footer_top']) : ?>
        <!-- FOOTER -->
        <footer id="footer">
    
            <?php if ($page['footer_top']) : ?>
                <!-- FOOTER TOP -->
                <div class="footer_top">
                    <div class="container">
                        <div class="row">
                            <?php print render($page['footer_top']); ?>
                        </div>
                    </div>
                </div>
                <!-- END / FOOTER TOP -->
            <?php endif; ?>
    
            <?php if ($page['footer']) : ?>
                <!-- FOOTER CENTER -->
                <div class="footer_center">
                    <div class="container">
                        <div class="row">
                            <?php print render($page['footer']); ?>
                        </div>
                    </div>
                </div>
                <!-- END / FOOTER CENTER -->
            <?php endif; ?>
    
            <?php if(theme_get_setting('info_text')) : ?>
                <!-- FOOTER BOTTOM -->
                <div class="footer_bottom">
                    <div class="container">
                        <p><?php print theme_get_setting('info_text'); ?></p>
                    </div>
                </div>
                <!-- END / FOOTER BOTTOM -->
            <?php endif; ?>
    
        </footer>
        <!-- END / FOOTER -->
    <?php endif; ?>
</div>
<!-- END / PAGE WRAP -->
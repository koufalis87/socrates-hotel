<!-- PRELOADER -->
<div id="preloader">
    <span class="preloader-dot"></span>
</div>
<!-- END / PRELOADER -->


<!-- PAGE WRAP -->
<div id="page-wrap">
    
    <?php
		$default_value = theme_get_setting('404_image_file_uploaded');
		$media_file = array('fid' => isset($default_value['fid']) ? intval($default_value['fid']) : 0);
		if ($media_file['fid'] && ($media_file = file_load($media_file['fid']))) {
		  $media_file = get_object_vars($media_file);
		}
	?>
    <section class="section-404 bg-5" style="background-image:url(<?php print file_create_url($media_file['uri']); ?>);">
        <div class="awe-overlay"></div>
        <div class="page-404 text-center">
            <a href="<?php print base_path(); ?>"><img src="<?php print $logo; ?>" alt=""></a>
            <h1><?php print theme_get_setting('404_title'); ?></h1> 
            <?php print theme_get_setting('404_subtitle'); ?>
            
            <div class="search-404">
                <?php print render($page['content']); ?>
            </div>
            
            <p><?php print theme_get_setting('404_link'); ?> <a href="<?php print base_path(); ?>"><?php print t('Home Page'); ?></a> </p>
        </div>

    </section>

</div>
<!-- END / PAGE WRAP -->
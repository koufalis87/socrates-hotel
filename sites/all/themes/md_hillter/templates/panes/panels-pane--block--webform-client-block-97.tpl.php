<div id="guest-book-popup">
    <div class="guest-book-form">
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
            <h2><?php print $title; ?></h2>
            <p>Your feedback means the world to us, it's how we improve our level of service. Feel free to share your experience if you've stayed with us before.</p>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php print render($content); ?>
    </div>
</div>
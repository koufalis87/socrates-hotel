<?php
/**
 * @file panels-pane.tpl.php
 * Main panel pane template
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>
<?php if ($pane_prefix): ?>
  <?php print $pane_prefix; ?>
<?php endif; ?>
<div class="<?php print $classes; ?>" <?php print $id; ?> <?php print $attributes; ?>>
  <?php if ($admin_links): ?>
    <?php print $admin_links; ?>
  <?php endif; ?>
  
    <?php if($pane->configuration['display'] == "home_room") : ?>
    	<!-- ACCOMD ODATIONS -->
        <section class="section-accomd <?php if($bg_effect == "parallax") print "awe-parallax"; ?>" <?php if($bg_image) print 'style="background-image:url('.$bg_image.')"'; ?>>
            <div class="awe-overlay"></div>
    
            <div class="container">
                <div class="accomd-modations">
                    <div class="row">
                        <?php print render($title_prefix); ?>
                        <?php if ($title): ?>
                            <div class="col-md-3 col-lg-3">
                                <div class="accomd-modations-header">
                                    <h2 class="heading"><?php print $title; ?></h2>
                                    <?php if($subtitle) : ?>
                                    	<p><?php print $subtitle; ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php print render($title_suffix); ?>
                        <div class="col-md-8 col-md-offset-1">
                            <?php print render($content); ?>
                        </div>
                    </div>
                </div>
            </div>
    
        </section>
        <!-- END / ACCOMD ODATIONS -->
	<?php elseif($pane->configuration['display'] == "home2_room") : ?>
    	<!-- ACCOMMODATIONS -->
        <section class="section-accommo_1 bg-white">
            <div class="container">
                <div class="accomd-modations_1">
                    <?php print render($title_prefix); ?>
                    <?php if ($title): ?>
                        <h2 class="heading"><?php print $title; ?></h2>
                        <?php if($subtitle) : ?>
                            <p><?php print $subtitle; ?></p>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php print render($title_suffix); ?>
                    <?php print render($content); ?>
                </div>
            </div>
        </section>
        <!-- ACCOMMODATIONS -->
	<?php else : ?>
    	<!-- ROOM -->
        <section class="section-room bg-white">
            <div class="container">
                <?php print render($title_prefix); ?>
                <?php if ($title): ?>
                    <h2 class="heading"><?php print $title; ?></h2>
                <?php endif; ?>
                <?php print render($title_suffix); ?>
                <?php print render($content); ?>
            </div>
        </section>
    <?php endif; ?>

  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <div class="more-link">
      <?php print $more; ?>
    </div>
  <?php endif; ?>
</div>
<?php if ($pane_suffix): ?>
  <?php print $pane_suffix; ?>
<?php endif; ?>

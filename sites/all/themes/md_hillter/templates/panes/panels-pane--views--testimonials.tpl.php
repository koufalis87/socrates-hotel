<?php
/**
 * @file panels-pane.tpl.php
 * Main panel pane template
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>
<?php if ($pane_prefix): ?>
  <?php print $pane_prefix; ?>
<?php endif; ?>
<div class="<?php print $classes; ?>" <?php print $id; ?> <?php print $attributes; ?>>
  <?php if ($admin_links): ?>
    <?php print $admin_links; ?>
  <?php endif; ?>
  
  	<?php if($pane->configuration['display'] == "default_testi") : ?>
    	<!-- HOME GUEST BOOK -->
        <section class="section-home-guestbook awe-parallax bg-13">
    
            <div class="awe-overlay"></div>
    
            <div class="container">
                <div class="home-guestbook">
                        
                    <div class="row">
                        <?php print render($title_prefix); ?>
                        <?php if ($title): ?>
                            <div class="col-md-3 col-lg-2 col-lg-offset-1 col-lg-push-9 col-md-push-9">
                                <div class="guestbook-header">
                                    <h2 class="heading"><?php print $title; ?></h2>
                                    <?php if($subtitle) : ?>
                                    	<p><?php print $subtitle; ?></p>
                                    <?php endif; ?>
                                    <a href="<?php print $pane->configuration['url'] ?>" class="awe-btn awe-btn-default"><?php print t('VIEW MORE'); ?></a>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php print render($title_suffix); ?>
                        
                        <div class="col-md-9 col-lg-9 col-lg-pull-3 col-md-pull-3">
                            <?php print render($content); ?>
                        </div>
                    </div>
    
                </div>
            </div>
    
        </section>
        <!-- END / HOME GUEST BOOK -->
    <?php else : ?>
    	<!-- GUEST BOOK -->
        <section class="section-guest-book">
            <div class="container">
                <div class="guest-book">
                    <?php print render($title_prefix); ?>
                    <?php if ($title): ?>
                        <h2 class="heading"><?php print $title; ?></h2>
                    <?php endif; ?>
                    <?php print render($title_suffix); ?>
                    <?php print render($content); ?>
                </div>
            </div>
        </section>
    <?php endif; ?>

  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <div class="more-link">
      <?php print $more; ?>
    </div>
  <?php endif; ?>
</div>
<?php if ($pane_suffix): ?>
  <?php print $pane_suffix; ?>
<?php endif; ?>

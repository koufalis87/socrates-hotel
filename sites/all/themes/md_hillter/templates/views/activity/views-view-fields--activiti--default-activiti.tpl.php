<!-- ITEM -->
<div class="item-isotope <?php $string = str_replace(' ', '', strtolower($fields['field_activity_categories']->content)); $string = str_replace('//', ' ', $string); $string = str_replace('&amp;', '-', $string); $string = str_replace('/', '-', $string); print $string; ?> col-xs-6 col-sm-6 col-md-4 col-lg-4">
    <div class="activiti_item">
        <div class="img">
            <a href="<?php print $fields['path']->content; ?>"><img src="<?php print $fields['field_activity_thumbnail']->content; ?>" alt=""></a>
        </div>
        <div class="text">
            <h2><a href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h2>
            <p><?php print $fields['body']->content; ?></p>
            <a href="<?php print $fields['path']->content; ?>" class="view-more"><?php print t('VIEW MORE'); ?> <i class="fa fa-chevron-right"></i></a>
        </div>
    </div>
</div>
<!-- END / ITEM -->
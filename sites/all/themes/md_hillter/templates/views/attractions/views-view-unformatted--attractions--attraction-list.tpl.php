<h2 class="attraction_heading"><?php print t('ATTRACTIONS'); ?> <span class="attraction-icon-drop fa fa-angle-down"></span></h2>
<div class="attraction_sidebar-content">
	<h3 class="attraction_title"><i class="fa fa-map-marker"></i><?php print $view->description; ?></h3>
	<ul class="attraction_location" id="attraction_location">
		<?php foreach ($rows as $id => $row): ?>
           <?php print $row; ?>
        <?php endforeach; ?>
    </ul>
</div>
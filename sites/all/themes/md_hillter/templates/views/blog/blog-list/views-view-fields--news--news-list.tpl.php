<!-- POST ITEM -->
<article class="post">

    <div class="entry-media">
        <?php if($fields['field_blog_multimedia']->content) : ?>
			<?php print $fields['field_blog_multimedia']->content; ?>    
        <?php else : ?>
            <a href="<?php print $fields['path']->content; ?>" class="post-thumbnail">
                <img src="<?php print $fields['field_blog_images']->content; ?>" alt="">
            </a>
        <?php endif; ?>

        <span class="posted-on"><strong><?php print $fields['created']->content; ?></strong><?php print $fields['created_1']->content; ?></span>

    </div>
    
    <div class="entry-header">
        <h2 class="entry-title"><a href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h2>
    </div>

    <div class="entry-content">
        <p><?php print $fields['body']->content; ?></p>
    </div>

    <div class="entry-footer">
        <p class="entry-meta">

            <span class="entry-author">
                <span class="screen-reader-text"><?php print t('Posted by'); ?> </span>
                <a href="<?php print base_path() . 'user/' . $fields['uid']->content; ?>" class="entry-author-link">
                    <span class="entry-author-name"><?php print $fields['name']->content; ?></span>
                </a>
            </span>

            <span class="entry-categories">
                <?php print $fields['field_blog_categories']->content; ?>
            </span>

            <span class="entry-comments-link">
                <a href="#"><?php print $fields['comment_count']->content; ?> <?php print t('Comments'); ?></a>
            </span>

            <span class="entry-tags">
                <span class="screen-reader-text"><i class="fa fa-tags"></i></span>
                <?php print $fields['field_blog_tags']->content; ?>
            </span>
        </p>
    </div>

</article>
<!-- END / POST ITEM -->
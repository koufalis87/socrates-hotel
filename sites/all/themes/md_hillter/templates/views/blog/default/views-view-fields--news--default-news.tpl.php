<!-- ITEM -->
<div class="col-md-12">
    <div class="news-item">
        <div class="img">
            <?php if($fields['field_blog_multimedia']->content) : ?>
            	<?php print str_replace('&', '&amp;', str_replace('frameborder="0"', '', $fields['field_blog_multimedia']->content)); ?>    
            <?php else : ?>
            	<a href="<?php print $fields['path']->content; ?>">
                    <img src="<?php print $fields['field_blog_images']->content; ?>" alt="">
                </a>
            <?php endif; ?>
        </div>
        <div class="text">
            <span class="date"><?php print $fields['created']->content; ?></span>
            <h2><a href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h2>
            <a href="<?php print $fields['path']->content; ?>" class="read-more">[ <?php print t('Read More'); ?> ]</a>
        </div>
    </div>
</div>
<!-- END / ITEM -->
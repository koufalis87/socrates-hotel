<li>
    <div class="img">
        <a href="<?php print $fields['path']->content; ?>"><img src="<?php print $fields['field_blog_images']->content; ?>" alt=""></a>
    </div>
    <div class="text">
        <a href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a>
        <span class="date"><?php print t('at'); ?> <?php print $fields['created_1']->content; ?></span>
    </div>
</li>
<table class="table table-cart">
  <thead>
      <tr>
          <?php foreach ($header as $field => $label): ?>
			<th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
			  <?php print $label; ?>
            </th>
          <?php endforeach; ?>
      </tr>
  </thead>
  <tbody>
      <?php foreach ($rows as $row_count => $row): ?>
        <tr <?php if ($row_classes[$row_count]) { print 'class="' . implode(' ', $row_classes[$row_count]) .'"';  } ?>>
          <?php foreach ($row as $field => $content): ?>
            <?php if ($field == 'line_item_label') : ?>
              <td <?php if ($field_classes[$field][$row_count]) { print 'class="td-item '. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
                <div class="info">
                    <?php if($field == 'line_item_label') : ?><?php print $content; ?><?php endif; ?>
                </div>
              </td>
            <?php endif; ?>
            <?php if ($field == 'commerce_unit_price') : ?>
              <td <?php if ($field_classes[$field][$row_count]) { print 'class="td-sub text-center '. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
                <?php print $content; ?>
              </td>
            <?php endif; ?>
            <?php if ($field == 'edit_delete') : ?>
              <td <?php if ($field_classes[$field][$row_count]) { print 'class="td-remove text-center '. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
                <?php print $content; ?>
              </td>
            <?php endif; ?>
            <?php if ($field == 'commerce_total') : ?>
              <td <?php if ($field_classes[$field][$row_count]) { print 'class="td-sub text-center '. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
                <?php print $content; ?>
              </td>
            <?php endif; ?>
          <?php endforeach; ?>
        </tr>
      <?php endforeach; ?>
  </tbody>
</table>
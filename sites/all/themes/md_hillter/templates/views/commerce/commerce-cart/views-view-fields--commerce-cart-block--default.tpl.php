<!-- ITEM -->
<div class="cart-item">
    <div class="text">
        <a href="#"><?php print $fields['line_item_title']->content ?></a>
        <p><b><?php print $fields['commerce_total']->content ?></b></p>
    </div>
    <a href="#" class="remove"><i class="fa fa-close"></i><?php print $fields['edit_delete']->content ?></a>
</div>
<!-- END / ITEM -->
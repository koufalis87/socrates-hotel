<!-- ITEM -->
<div class="col-xs-6">
    <div class="event-item">
        <div class="img">
            <a href="<?php print $fields['path']->content; ?>">
                <img src="<?php print $fields['field_event_thumbnail']->content; ?>" alt="">
            </a>
        </div>
        <div class="text">
            <div class="text-cn">
                <h2><?php print $fields['title']->content; ?></h2>
                <a href="<?php print $fields['path']->content; ?>" class="awe-btn awe-btn-12"><?php print t('VIEW MORE'); ?></a>
            </div>
        </div>
    </div>
</div>
<!-- END / ITEM -->
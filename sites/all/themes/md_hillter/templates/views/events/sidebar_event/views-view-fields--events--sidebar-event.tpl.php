<li>
    <span class="event-date">
        <strong><?php print $fields['created']->content; ?></strong>
        <?php print $fields['created_1']->content; ?>
    </span>
    <div class="text">
        <a href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a>
        <span class="date"><?php print t('at'); ?> <?php print $fields['created_2']->content; ?></span>
    </div>
</li>
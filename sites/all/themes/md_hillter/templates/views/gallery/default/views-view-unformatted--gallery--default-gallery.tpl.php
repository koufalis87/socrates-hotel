<!--Get categories-->
<?php
$voca = taxonomy_vocabulary_machine_name_load('gallery_categories');
$terms = taxonomy_get_tree($voca->vid);
?>
<!-- FILTER -->
<div class="gallery-cat text-center">
    <ul class="list-inline">
        <li class="active"><a href="#" data-filter="*"><?php print t('All'); ?></a></li>
        <?php foreach ($terms as $term): ?>
          <li><a href="#" data-filter=".<?php $string = str_replace(' ', '', strtolower($term->name)); $string = str_replace('&', '-', $string); $string = str_replace('/', '-', $string); print $string; ?>"><?php print $term->name; ?></a></li>
        <?php endforeach; ?>
    </ul>
</div>
<!-- END / FILTER -->
<!-- GALLERY CONTENT -->
<div class="gallery-content">
	<div class="row">
    	<div class="gallery-isotope">
			<!-- ITEM SIZE -->
            <div class="item-size col-xs-6 col-sm-4 col-md-3 col-lg-2"></div>
            <!-- END / ITEM SIZE -->
			<?php foreach ($rows as $id => $row): ?>
                <?php print $row; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php $path = ""; $type = ""; ?>
<?php if($row->field_field_gallery_multimedia[0]['rendered']['file']['#theme'] == "image_formatter") : ?>
    <?php $path = file_create_url($row->field_field_gallery_multimedia[0]['rendered']['file']['#item']['uri']); ?>
    <?php $type = "mfp-image"; ?>
<?php elseif($row->field_field_gallery_multimedia[0]['rendered']['file']['#theme'] == "media_soundcloud_audio") : ?>
	<?php $type = "mfp-iframe"; ?>
    <?php $path = "//w.soundcloud.com/player/?url=" . file_create_url($row->field_field_gallery_multimedia[0]['rendered']['file']['#uri']) . "&amp;visual=1"; ?>
<?php else : ?>
    <?php
        if(strpos($row->field_field_gallery_multimedia[0]['rendered']['file']['#uri'], 'vimeo') !== FALSE) :
            $uri = explode('v/',$row->field_field_gallery_multimedia[0]['rendered']['file']['#uri']);
            $path = "http://vimeo.com/" . $uri[1];
        else :
            $uri = explode('v/',$row->field_field_gallery_multimedia[0]['rendered']['file']['#uri']);
            $path = "http://www.youtube.com/watch?v=" . $uri[1];
        endif;
		$type = "mfp-iframe";
    ?>
<?php endif; ?>
<!-- ITEM -->
<div class="item-isotope <?php $string = str_replace(' ', '', strtolower($fields['field_gallery_categories']->content)); $string = str_replace('//', ' ', $string); $string = str_replace('&amp;', '-', $string); $string = str_replace('/', '-', $string); print $string; ?> col-xs-6 col-sm-6 col-md-4 col-lg-3">
    <div class="gallery_item">
        <a href="<?php print $path; ?>" class="gallery-popup <?php print $type; ?>" title="<?php print $fields['title']->content; ?>">
            <img src="<?php print $fields['field_gallery_thumbnail']->content; ?>" alt="">
        </a>
        <h6 class="text"><?php print $fields['title']->content; ?></h6>
        <?php if($type == "mfp-iframe"): ?><span class="icon"><i class="fa hillter-icon-media-play"></i></span><?php endif; ?>
    </div>
</div>
<!-- END / ITEM -->
<?php 
	$data = _md_rooms_get_rooms_detail($fields['nid']->content); 
	$cur = commerce_multicurrency_get_user_currency_code();
	$cur_load = commerce_currency_load($cur);
		
	$price = commerce_currency_convert($data['price'], commerce_default_currency(), $cur);
	$price = round($price, 2);
	
	$cur   = $cur_load;
	$data['price'] = $price;
?>
<!-- ITEM -->
<div class="col-xs-6">
    <div class="accomd-modations-room">
        <div class="img">
            <a href="<?php print $fields['path']->content ?>"><img src="<?php print $fields['field_unit_thumbnail']->content ?>" alt="<?php print $fields['title']->content ?>"></a>
        </div>
        <div class="text">
            <h2><a href="<?php print $fields['path']->content ?>"><?php print $fields['title']->content ?></a></h2>
            <p class="price">
                <span class="amout"><?php print $cur['symbol'].$data['price'] ?></span>/<?php print t('night(s)'); ?>
            </p>
        </div>
    </div>
</div>
<!-- END / ITEM -->
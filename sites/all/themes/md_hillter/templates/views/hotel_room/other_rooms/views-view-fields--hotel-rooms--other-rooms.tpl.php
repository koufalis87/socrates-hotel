<!-- ITEM -->
<div class="col-sm-6 col-md-4 col-lg-3">
    <div class="room-compare_item">
        <div class="img">
            <a href="<?php print $fields['path']->content ?>"><img src="<?php print $fields['field_unit_thumbnail']->content ?>" alt=""></a>
        </div>  
    
        <div class="text">
            <h2><a href=""><?php print $fields['title']->content ?></a></h2>
    
            <ul>
                <li><i class="hillter-icon-person"></i> <?php print t('Max') ?>: <?php print $fields['field_unit_max_person']->content ?> <?php print t('Person(s)'); ?></li>
                <li><i class="hillter-icon-bed"></i> <?php print t('Bed') ?>: <?php print $fields['field_unit_bed']->content ?></li>
                <li><i class="hillter-icon-view"></i> <?php print t('View') ?>: <?php print $fields['field_unit_view']->content ?></li>
            </ul>
    
            <a href="<?php print $fields['path']->content ?>" class="awe-btn awe-btn-default"><?php print t('View details'); ?></a>
        </div>    
    </div>
</div>
<!-- END / ITEM -->
<?php 
	$data = _md_rooms_get_rooms_detail($fields['nid']->content); 
	$cur = commerce_multicurrency_get_user_currency_code();
	$cur_load = commerce_currency_load($cur);
		
	$price = commerce_currency_convert($data['price'], commerce_default_currency(), $cur);
	$price = round($price, 2);
	
	$cur   = $cur_load;
	$data['price'] = $price;
?>
<!-- ITEM -->
<div class="col-md-6">
    <div class="room_item-1">
    
        <h2><a href="<?php print $fields['path']->content ?>"><?php print $fields['title']->content ?></a></h2>
    
        <div class="img">
            <a href="<?php print $fields['path']->content ?>"><img src="<?php print $fields['field_unit_thumbnail']->content ?>" alt=""></a>
        </div>
    
        <div class="desc">
            <p><?php print $fields['field_unit_description']->content ?></p>
            <ul>
                <li><?php print t('Max') ?>: <?php print $fields['field_unit_max_person']->content ?> <?php print t('Person(s)'); ?></li>
                <li><?php print t('Size') ?>: <?php print $fields['field_unit_size_square']->content ?> m2 / <?php print $fields['field_unit_size_ft']->content ?> ft2</li>
                <li><?php print t('View') ?>: <?php print $fields['field_unit_view']->content ?></li>
                <li><?php print t('Bed') ?>: <?php print $fields['field_unit_bed']->content ?></li>
            </ul>
        </div>
    
        <div class="bot">
            <span class="price"><?php print t('Starting'); ?> <span class="amout"><?php print $cur['symbol'].$data['price'] ?></span> /<?php print t('night(s)'); ?></span>
            <a href="<?php print $fields['path']->content ?>" class="awe-btn awe-btn-13"><?php print t('View details'); ?></a>
        </div>
    
    </div>
</div>
<!-- END / ITEM -->
<?php $count = 0; ?>
<div class="room-wrap-3">
	<div class="row">
    	<div class="col-lg-10 col-lg-offset-1">
			<?php foreach ($rows as $id => $row): ?>
                <?php $count++; ?>
				<!-- ITEM -->
				<div class="room_item-3 <?php if($count%2 == 0) print "thumbs-right"; ?>">
					<?php print $row; ?>
                </div>    
				<!-- END / ITEM -->
            <?php endforeach; ?>
        </div>
    </div>
</div>
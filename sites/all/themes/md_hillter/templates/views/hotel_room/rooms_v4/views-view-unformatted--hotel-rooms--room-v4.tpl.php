<?php $count = 0; ?>
<div class="room-wrap-4">
	<?php foreach ($rows as $id => $row): ?>
		<?php $count++; ?>
        <!-- ITEM -->
        <div class="room_item-4 <?php if($count%2 == 0) print "img-right"; ?>">
            <?php print $row; ?>
        </div>    
        <!-- END / ITEM -->
    <?php endforeach; ?>
</div>
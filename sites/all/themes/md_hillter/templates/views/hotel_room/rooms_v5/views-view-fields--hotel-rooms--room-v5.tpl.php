<?php 
	$data = _md_rooms_get_rooms_detail($fields['nid']->content); 
	$cur = commerce_multicurrency_get_user_currency_code();
	$cur_load = commerce_currency_load($cur);
		
	$price = commerce_currency_convert($data['price'], commerce_default_currency(), $cur);
	$price = round($price, 2);
	
	$cur   = $cur_load;
	$data['price'] = $price;
?>
<!-- ITEM -->
<div class="col-xs-6">
    <div class="room_item-5" data-background='<?php print $fields['field_unit_thumbnail']->content ?>'>

        <div class="room_item-forward">
            <h2><a href="<?php print $fields['path']->content ?>"><?php print $fields['title']->content ?></a></h2>
            <span class="price"><?php print t('Start from'); ?> <?php print $cur['symbol'].$data['price'] ?> <?php print t('per night'); ?></span>
        </div>

        <div class="room_item-back">
            <h3><?php print $fields['title']->content ?></h3>
            <span class="price"><?php print t('Start from'); ?> <?php print $cur['symbol'].$data['price'] ?> <?php print t('per night'); ?></span>
            <p><?php print $fields['field_unit_description']->content ?></p>
            <a href="<?php print $fields['path']->content ?>" class="awe-btn awe-btn-13"><?php print t('View details'); ?></a>
        </div>

    </div>
</div>
<!-- END / ITEM -->
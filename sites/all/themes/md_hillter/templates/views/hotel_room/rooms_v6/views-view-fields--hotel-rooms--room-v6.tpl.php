<?php 
	$data = _md_rooms_get_rooms_detail($fields['nid']->content); 
	$cur = commerce_multicurrency_get_user_currency_code();
	$cur_load = commerce_currency_load($cur);
		
	$price = commerce_currency_convert($data['price'], commerce_default_currency(), $cur);
	$price = round($price, 2);
	
	$cur   = $cur_load;
	$data['price'] = $price;
?>
<!-- ITEM -->
<div class="room_item-6" data-background="<?php print $fields['field_unit_thumbnail']->content ?>">

    <div class="text">
        <h2><a href="<?php print $fields['path']->content ?>"><?php print $fields['title']->content ?></a></h2>
        <span class="price"><?php print t('Start from'); ?> <span class="amout"><?php print $cur['symbol'].$data['price'] ?></span> <?php print t('per night'); ?></span>
        <p><?php print $fields['field_unit_description']->content ?></p>
        <ul>
            <li><?php print t('Max') ?>: <?php print $fields['field_unit_max_person']->content ?> <?php print t('Person(s)'); ?></li>
            <li><?php print t('Size') ?>: <?php print $fields['field_unit_size_square']->content ?> m2 / <?php print $fields['field_unit_size_ft']->content ?> ft2</li>
            <li><?php print t('View') ?>: <?php print $fields['field_unit_view']->content ?></li>
            <li><?php print t('Bed') ?>: <?php print $fields['field_unit_bed']->content ?></li>
        </ul>
        <a href="<?php print $fields['path']->content ?>" class="awe-btn awe-btn-13"><?php print t('View details'); ?></a>
    </div>

</div>
<!-- END / ITEM -->
<?php 
	$data = _md_rooms_get_rooms_detail($fields['nid']->content); 
	$cur = commerce_multicurrency_get_user_currency_code();
	$cur_load = commerce_currency_load($cur);
		
	$price = commerce_currency_convert($data['price'], commerce_default_currency(), $cur);
	$price = round($price, 2);
	
	$cur   = $cur_load;
	$data['price'] = $price;
?>
<!-- ITEM -->
<div class="item">
    <div class="img">
        <img src="<?php print $fields['field_unit_thumbnail']->content ?>" alt="">
    </div>
    <div class="text">
        <h2><?php print $fields['title']->content ?></h2>
        <p class="price"><?php print $cur['symbol'].$data['price'] ?></p>
        <a href="<?php print $fields['path']->content ?>" class="awe-btn awe-btn-12"><?php print t('VIEW MORE'); ?></a>
    </div>
</div>
<!-- END / ITEM -->
<!-- ITEM -->
<div class="item-isotope <?php $string = str_replace(' ', '', strtolower($fields['field_res_group']->content)); $string = str_replace('//', ' ', $string); $string = str_replace('&amp;', '-', $string); $string = str_replace('/', '-', $string); print $string; ?> col-xs-6 col-sm-6 col-md-4 col-lg-3">
    <div class="gallery_item">
        <a href="<?php print file_create_url($fields['field_res_thumbnail']->content); ?>" class="gallery-popup mfp-image">
            <img src="<?php print image_style_url('restaurant_1', $fields['field_res_thumbnail']->content); ?>" alt="">
        </a>
    </div>
</div>
<!-- END / ITEM -->
<!-- ITEM -->
<div class="restaurant_item small-thumbs">

    <div class="img">
        <a href="javascript:void(0)"><img src="<?php print $fields['field_res_thumbnail']->content; ?>" alt=""></a>
    </div>

    <div class="text">
        <h2><a href="javascript:void(0)"><?php print $fields['title']->content; ?></a></h2>

        <div class="desc"><?php print $fields['field_res_body']->content; ?></div>

        <p class="price">
            <span class="amout"><?php print $fields['commerce_price']->content; ?></span>
        </p>
    </div>

</div>
<!-- END / ITEM -->
<!-- ITEM -->
<div class="col-md-4 col-sm-6">
    <div class="restaurant_item">
        <div class="text">
            <h2><a href="javascript:void(0)"><?php print $fields['title']->content; ?></a></h2>
    
            <div class="desc"><?php print $fields['field_res_body']->content; ?></div>
    
            <p class="price">
                <span class="amout"><?php print $fields['commerce_price']->content; ?></span>
            </p>
        </div>
    </div>
</div>
<!-- END / ITEM -->
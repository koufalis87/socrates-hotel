<?php
$term = taxonomy_term_load($fields['tid']->content); // load term object
$term_uri = taxonomy_term_uri($term); // get array with path
$term_title = taxonomy_term_title($term);
$term_path = $term_uri['path'];
?>
<li><a href="<?php print base_path().$term_path ?>"><?php print $fields['name']->content ?> (<?php print $fields['nid']->content ?>)</a></li>
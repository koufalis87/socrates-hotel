<!-- ITEM -->
<div class="col-xs-6 col-md-3">
    <div class="team_item text-center">

        <div class="img">
            <a href=""><img src="<?php print $fields['field_team_image']->content;?>" alt=""></a>
        </div> 

        <div class="text">
            <h2><?php print $fields['title']->content; ?></h2>
            <span><?php print $fields['field_team_position']->content;?></span>
            <p><?php print $fields['field_team_description']->content;?></p>
            <div class="team-share">
                <?php if(isset($row->field_field_team_socials)) : ?>
					<?php for($i=0; $i < count($row->field_field_team_socials); $i++) : ?>
                        <?php
                            $id = $row->field_field_team_socials[$i]['raw']['value'];
                            $icon = $row->field_field_team_socials[$i]['rendered']['entity']['field_collection_item'][$id]['field_team_socials_icon']['#items'][0]['value'];
                            $link = $row->field_field_team_socials[$i]['rendered']['entity']['field_collection_item'][$id]['field_team_socials_link']['#items'][0]['value'];
                        ?>
                        <a href="<?php print $link; ?>"><i class="fa <?php print $icon; ?>"></i></a>
                    <?php endfor; ?>
                <?php endif; ?>
            </div>
        </div>

    </div>
</div>
<!-- END / ITEM -->
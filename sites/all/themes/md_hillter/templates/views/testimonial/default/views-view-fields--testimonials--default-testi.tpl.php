<!-- ITEM -->
<div class="guestbook-item">
    <div class="img">
        <img src="<?php print $fields['field_testi_image']->content ?>" alt="">
    </div>

    <div class="text">
        <p><?php print $fields['field_testi_quote']->content ?></p>
        <span><strong><?php print $fields['field_testi_name']->content ?></strong><?php print $fields['field_testi_description']->content ?></span>
    </div> 
</div>
<!-- ITEM -->
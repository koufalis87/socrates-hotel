<!-- ITEM -->
<div class="item-masonry col-xs-6 col-md-4">
    <div class="guest-book_item">
        <span class="icon hillter-icon-quote-left"></span>
        <h2><?php print $fields['title']->content ?></h2>
        <p><?php print $fields['field_testi_quote']->content ?></p>
        <span><b><?php print $fields['field_testi_name']->content ?></b> - <?php print $fields['field_testi_description']->content ?></span>
    </div>
</div>
<!-- END / ITEM -->